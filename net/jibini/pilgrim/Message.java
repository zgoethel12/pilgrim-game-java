package net.jibini.pilgrim;

import net.jibini.platformer.DeltaTracker;

/**
 *
 * @author zgoethel12
 */
public class Message
{
	public String msg;
	double life;

	DeltaTracker tracker = new DeltaTracker();

	public Message(String msg)
	{
		this.msg = msg;
		life = 10;
	}

	public void update()
	{
		life -= tracker.getDeltaTime();

		if (life <= 0 && Main.instance.game.messages.contains(this))
		{
			Main.instance.game.messages.remove(this);
		}
	}
}
