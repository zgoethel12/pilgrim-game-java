package net.jibini.pilgrim.gui;

import net.jibini.pilgrim.Main;
import net.jibini.pilgrim.assets.Sounds;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.opengl.TextureImpl;

/**
 *
 * @author zgoethel12
 */
import static org.lwjgl.opengl.GL11.*;

public class GuiElementButton
{

	public float x, y, width, height;
	public String text;
	public boolean downOn = false, hovering = false, m_last = false, m_just;
	public OnClickListener onClick = null;

	public GuiElementButton(float x, float y, float width, String text)
	{

		this.x = x;
		this.y = y;
		if (Main.instance.fontRenderer.getWidth(text) + 10 > width)
		{
			this.width = Main.instance.fontRenderer.getWidth(text) + 20;
		} else
		{
			this.width = width;
		}
		height = Main.instance.fontRenderer.getHeight() + 10;
		this.text = text;

	}

	public void render()
	{

		update();

		TextureImpl.bindNone();
		if (downOn)
		{
			glColor3f(0.5f, 0.5f, 0.5f);
		} else if (hovering)
		{
			glColor3f(0.25f, 0.25f, 0.25f);
		} else
		{
			glColor3f(0.125f, 0.125f, 0.125f);
		}

		glBegin(GL_QUADS);
		glVertex2f(x, y + height);
		glVertex2f(x + width, y + height);
		glVertex2f(x + width, y);
		glVertex2f(x, y);
		glEnd();

		glColor3f(1, 1, 1);
		glPushMatrix();
		glTranslatef(0, 0, 1);
		Main.instance.fontRenderer.drawString((x + x + width) / 2 - Main.instance.fontRenderer.getWidth(text) / 2,
				y + 5, text);
		glPopMatrix();
		TextureImpl.bindNone();

	}

	public void update()
	{

		boolean m_now = Mouse.isButtonDown(0);
		float mx = Mouse.getX();
		float my = Display.getHeight() - Mouse.getY();
		hovering = mx > x && mx < x + width && my > y && my < y + height;
		if (m_now && !m_last && hovering)
		{
			downOn = true;
		}
		if (!m_now)
		{
			if (hovering && downOn)
			{
				if (onClick != null)
				{
					Sounds.PICK.play(1.5f, 1);
					onClick.onClick();
				}
			}
			downOn = false;
		}
		m_last = m_now;

	}

}
