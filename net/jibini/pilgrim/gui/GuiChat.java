package net.jibini.pilgrim.gui;

import net.jibini.networking.client.Client;
import net.jibini.networking.server.Server;
import net.jibini.pilgrim.Main;
import net.jibini.pilgrim.Message;
import net.jibini.pilgrim.networking.packets.PacketMessage;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.opengl.TextureImpl;

/**
 *
 * @author zgoethel12
 */
import static org.lwjgl.opengl.GL11.*;

public class GuiChat extends Gui
{

	GuiElementTextBox input;
	boolean removedT = false;

	public GuiChat()
	{
		controls();
	}

	@Override
	public void render()
	{

		update();

		glPushMatrix();
		glTranslatef(0, 0, 10);

		if (Display.wasResized())
		{
			controls();
		}

		glTranslatef(0, 0, 1);

		TextureImpl.bindNone();
		input.render();

		glPopMatrix();

		if (!removedT)
		{
			input.text = input.text.substring(1);
			removedT = true;
		}

	}

	public void update()
	{
		if (Main.instance.pressedI.contains(Keyboard.KEY_RETURN))
		{
			PacketMessage p = new PacketMessage(Main.instance.settings.displayName, input.text);
			if (Main.instance.game.isServer)
			{
				Server.globalPacket(p, true);
				Main.instance.game.messages.add(new Message(p.user + ": " + p.msg));
			} else
			{
				Client.sendPacket(p);
			}
			Main.instance.gui = null;
		}
	}

	public final void controls()
	{

		input = new GuiElementTextBox(10, 10, 500);
		input.x = Display.getWidth() - input.width - 10;
		input.y = Display.getHeight() - input.height - 5;
		input.focus = true;

	}

}
