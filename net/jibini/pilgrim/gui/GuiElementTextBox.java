package net.jibini.pilgrim.gui;

import net.jibini.pilgrim.Main;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.opengl.TextureImpl;

/**
 *
 * @author zgoethel12
 */
import static org.lwjgl.opengl.GL11.*;

public class GuiElementTextBox extends Gui
{

	public float x, y, width, height;
	public String text = "";
	public boolean focus = false;

	public GuiElementTextBox(float x, float y, float width)
	{

		this.x = x;
		this.y = y;
		if (Main.instance.fontRenderer.getWidth(text) + 10 > width)
		{
			this.width = Main.instance.fontRenderer.getWidth(text) + 20;
		} else
		{
			this.width = width;
		}
		height = Main.instance.fontRenderer.getHeight() + 10;

	}

	@Override
	public void render()
	{
		if (focus)
		{
			update();
		}

		if (focus)
		{
			glColor3f(0.5f, 0.5f, 0.5f);
		} else
		{
			glColor3f(0.25f, 0.25f, 0.25f);
		}
		glBegin(GL_QUADS);
		glVertex2f(x, y + height);
		glVertex2f(x + width, y + height);
		glVertex2f(x + width, y);
		glVertex2f(x, y);
		glEnd();
		glColor3f(1, 1, 1);

		glPushMatrix();
		glTranslatef(0, 0, 1);
		Main.instance.fontRenderer.drawString(x + 10, y + 5, text);
		glPopMatrix();
		TextureImpl.bindNone();

	}

	public void update()
	{
		String accepted = ". _/|:,'\"()!@#$%^&*?~`-+\\=;<>";
		for (Character c : Main.instance.pressedC)
		{
			float textLength = Main.instance.fontRenderer.getWidth(text);
			if (textLength < width - 20 && (Character.isLetterOrDigit(c) || accepted.contains(String.valueOf(c))))
			{
				text += c;
			}
		}
		for (Integer i : Main.instance.pressedI)
		{
			if (i == Keyboard.KEY_DELETE || i == Keyboard.KEY_BACK)
			{
				if (text.length() > 0)
				{
					text = text.substring(0, text.length() - 1);
				}
			}
		}
	}

	@Override
	public void mouse(int button, boolean state)
	{
		float mx = Mouse.getX();
		float my = Display.getHeight() - Mouse.getY();
		boolean hovering = mx > x && mx < x + width && my > y && my < y + 50;
		if (button == 0 && state)
		{
			focus = hovering;
		}
	}

}
