package net.jibini.pilgrim.gui;

import net.jibini.pilgrim.Main;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.opengl.TextureImpl;

/**
 *
 * @author zgoethel12
 */
public class GuiDisplayMessage extends Gui
{

	public GuiElementButton back;
	public String msg;

	public GuiDisplayMessage(String msg)
	{
		this.msg = msg;
		controls();
	}

	@Override
	public void render()
	{
		float width = Main.instance.fontRenderer.getWidth(msg);
		float x = Display.getWidth() / 2 - width / 2;
		Main.instance.fontRenderer.drawString(x, Display.getHeight() / 2 - 20, msg);
		TextureImpl.bindNone();

		back.render();

		if (Display.wasResized())
		{
			controls();
		}
	}

	public final void controls()
	{
		back = new GuiElementButton(25, Display.getHeight() / 2 + 10, 0, "Back");
		back.width += 100;
		float width = back.width;
		back.x = Display.getWidth() / 2 - width / 2;

		back.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				Main.instance.gui = new GuiMainMenu();
			}
		};
	}

}
