package net.jibini.pilgrim.gui;

import net.jibini.pilgrim.Main;
import net.jibini.pilgrim.assets.Textures;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.opengl.TextureImpl;

/**
 *
 * @author zgoethel12
 */
import static org.lwjgl.opengl.GL11.*;

public class GuiMainMenu extends Gui
{

	public GuiElementButton singleplayer, multiplayer, options, quit;

	public GuiMainMenu()
	{
		controls();
	}

	@Override
	public void render()
	{
		Main.instance.fontRenderer.drawString(10, Display.getHeight() - (Main.instance.fontRenderer.getHeight() + 5),
				Main.instance.name + " " + Main.instance.version);
		Main.instance.fontRenderer.drawString(
				Display.getWidth() - (Main.instance.fontRenderer.getWidth(Main.instance.credit) + 10),
				Display.getHeight() - (Main.instance.fontRenderer.getHeight() + 5), Main.instance.credit);
		TextureImpl.bindNone();

		singleplayer.render();
		multiplayer.render();
		options.render();
		quit.render();

		Textures.LOGO.bind();
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2f(0, Display.getHeight() / 2);
		glTexCoord2f(1, 0);
		glVertex2f(Display.getWidth(), Display.getHeight() / 2);
		glTexCoord2f(1, -1);
		glVertex2f(Display.getWidth(), Display.getHeight() / 2 - Display.getWidth());
		glTexCoord2f(0, -1);
		glVertex2f(0, Display.getHeight() / 2 - Display.getWidth());
		glEnd();
		TextureImpl.bindNone();

		if (Display.wasResized())
		{
			controls();
		}
	}

	public final void controls()
	{
		singleplayer = new GuiElementButton(25, Display.getHeight() / 2 + 10, Display.getWidth() - 50, "Singleplayer");
		multiplayer = new GuiElementButton(25, Display.getHeight() / 2 + 45 + 10, Display.getWidth() - 50,
				"Multiplayer");
		options = new GuiElementButton(25, Display.getHeight() / 2 + 45 * 2 + 10, Display.getWidth() - 50, "Options");
		quit = new GuiElementButton(25, Display.getHeight() / 2 + 45 * 4 + 10, Display.getWidth() - 50, "Quit");

		options.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				Main.instance.gui = new GuiOptions();
			}
		};

		quit.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				Main.instance.manualClose = true;
			}
		};

		singleplayer.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				Main.instance.gui = new GuiLevelSelect(false, true);
			}
		};

		multiplayer.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				Main.instance.gui = new GuiMultiplayer();
			}
		};
	}

}
