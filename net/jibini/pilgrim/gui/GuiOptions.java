package net.jibini.pilgrim.gui;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.jibini.pilgrim.Main;
import net.jibini.pilgrim.humans.Humans;
import net.jibini.platformer.character.CharacterAnimation;
import net.jibini.platformer.character.CharacterAnimator;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureImpl;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

/**
 *
 * @author zgoethel12
 */
import static org.lwjgl.opengl.GL11.*;

public class GuiOptions extends Gui
{

	GuiElementTextBox displayName;
	GuiElementButton save, cancel, next, last, reducedParticles, particleNoClip, vignette;
	Texture character = null;
	String c;
	public boolean rP, pNC, v;

	public GuiOptions()
	{
		try
		{
			c = Main.instance.settings.character;
			character = TextureLoader.getTexture("PNG",
					ResourceLoader.getResourceAsStream("assets/textures/characters/" + c.toLowerCase() + ".png"),
					GL_NEAREST);
		} catch (IOException ex)
		{
			Logger.getLogger(GuiOptions.class.getName()).log(Level.SEVERE, null, ex);
		}
		controls();
	}

	@Override
	public void render()
	{
		if (Display.wasResized())
		{
			controls();
		}

		CharacterAnimator ca = new CharacterAnimator(character);
		ca.setCurrentAnimation(CharacterAnimation.WALK);
		ca.setCurrentDirection(CharacterAnimation.RIGHT);
		ca.current.stage = 7;

		ca.quad(25, 250, 200, -200);

		Main.instance.fontRenderer.drawString(45, displayName.y + 7, "Display Name: ");
		TextureImpl.bindNone();

		displayName.render();
		save.render();
		cancel.render();
		next.render();
		last.render();
		reducedParticles.render();
		particleNoClip.render();
		vignette.render();
	}

	public final void controls()
	{
		displayName = new GuiElementTextBox(200, 25, Display.getWidth() - 225);
		displayName.text = Main.instance.settings.displayName;
		save = new GuiElementButton(25, Display.getHeight() - (displayName.height * 2 + 35), Display.getWidth() - 50,
				"Save");
		cancel = new GuiElementButton(25, Display.getHeight() - (displayName.height + 25), Display.getWidth() - 50,
				"Cancel");
		last = new GuiElementButton(35, 260, 75, "<");
		next = new GuiElementButton(115, 260, 75, ">");
		rP = Main.instance.settings.reducedParticles;
		pNC = Main.instance.settings.particleNoClip;
		v = Main.instance.settings.vignette;
		reducedParticles = new GuiElementButton(210, 260 - (last.height + 10), 275,
				"Reduced Particles: " + ("" + rP).toUpperCase());
		particleNoClip = new GuiElementButton(210, 260, 275, "Particle Noclip: " + ("" + pNC).toUpperCase());
		vignette = new GuiElementButton(210, 260 - (last.height + 10) * 2, 275, "Vignette: " + ("" + v).toUpperCase());
		reducedParticles.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				rP = !rP;
				reducedParticles.text = "Reduced Particles: " + ("" + rP).toUpperCase();
			}
		};
		particleNoClip.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				pNC = !pNC;
				particleNoClip.text = "Particle Noclip: " + ("" + pNC).toUpperCase();
			}
		};
		vignette.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				v = !v;
				vignette.text = "Vignette: " + ("" + v).toUpperCase();
			}
		};
		save.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				Main.instance.settings.displayName = displayName.text;
				Main.instance.settings.character = c;
				Main.instance.settings.reducedParticles = rP;
				Main.instance.settings.particleNoClip = pNC;
				Main.instance.settings.vignette = v;
				Main.instance.saveConfig();
				Main.instance.gui = new GuiMainMenu();
			}
		};
		cancel.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				Main.instance.gui = new GuiMainMenu();
			}
		};
		last.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				int p = 0;
				int i = 0;
				for (String s : Humans.names)
				{
					if (s.equals(c))
					{
						p = i;
					}
					i++;
				}
				p--;
				if (p < 0)
				{
					p = Humans.names.length - 1;
				}
				c = Humans.names[p];
				try
				{
					character = TextureLoader.getTexture("PNG", ResourceLoader
							.getResourceAsStream("assets/textures/characters/" + c.toLowerCase() + ".png"), GL_NEAREST);
				} catch (IOException ex)
				{
					Logger.getLogger(GuiOptions.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		};
		next.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				int p = 0;
				int i = 0;
				for (String s : Humans.names)
				{
					if (s.equals(c))
					{
						p = i;
					}
					i++;
				}
				p++;
				if (p > Humans.names.length - 1)
				{
					p = 0;
				}
				c = Humans.names[p];
				try
				{
					character = TextureLoader.getTexture("PNG", ResourceLoader
							.getResourceAsStream("assets/textures/characters/" + c.toLowerCase() + ".png"), GL_NEAREST);
				} catch (IOException ex)
				{
					Logger.getLogger(GuiOptions.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		};
	}

	@Override
	public void mouse(int button, boolean state)
	{
		displayName.mouse(button, state);
	}

}
