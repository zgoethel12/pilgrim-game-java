package net.jibini.pilgrim.gui;

import net.jibini.pilgrim.Main;
import org.lwjgl.opengl.Display;

/**
 *
 * @author zgoethel12
 */
import static org.lwjgl.opengl.GL11.*;

public class GuiPause extends Gui
{

	GuiElementButton back, quit;

	public GuiPause()
	{
		controls();
	}

	@Override
	public void render()
	{

		glPushMatrix();
		glTranslatef(0, 0, 10);

		glColor4f(1, 1, 1, 0.25f);
		glBegin(GL_QUADS);
		glVertex2f(10, Display.getHeight() - 10);
		glVertex2f(430, Display.getHeight() - 10);
		glVertex2f(430, Display.getHeight() - (back.height * 2 + 40));
		glVertex2f(10, Display.getHeight() - (back.height * 2 + 40));
		glEnd();
		glColor4f(1, 1, 1, 1);

		if (Display.wasResized())
		{
			controls();
		}

		glTranslatef(0, 0, 1);

		back.render();
		quit.render();

		glPopMatrix();

	}

	public final void controls()
	{

		back = new GuiElementButton(20, 0, 400, "Back");
		back.y = Display.getHeight() - (back.height * 2 + 30);
		quit = new GuiElementButton(20, Display.getHeight() - (back.height + 20), 400, "Quit");

		back.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				Main.instance.gui = null;
			}
		};
		quit.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				Main.instance.exitGame();
				Main.instance.gui = new GuiMainMenu();
			}
		};

	}

}
