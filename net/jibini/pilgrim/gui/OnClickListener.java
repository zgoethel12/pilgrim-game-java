package net.jibini.pilgrim.gui;

/**
 *
 * @author zgoethel12
 */
public abstract class OnClickListener
{

	public abstract void onClick();

}
