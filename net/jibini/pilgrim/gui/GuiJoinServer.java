package net.jibini.pilgrim.gui;

import net.jibini.pilgrim.Main;
import org.lwjgl.opengl.Display;

/**
 *
 * @author zgoethel12
 */
public class GuiJoinServer extends Gui
{

	public GuiElementButton local, start, back;
	public GuiElementTextBox custom;

	public GuiJoinServer()
	{
		controls();
	}

	@Override
	public void render()
	{
		local.render();
		start.render();
		custom.render();
		back.render();

		if (Display.wasResized())
		{
			controls();
		}
	}

	public final void controls()
	{
		local = new GuiElementButton(25, 10, Display.getWidth() - 50, "Find Local Games");
		custom = new GuiElementTextBox(25, 90, Display.getWidth() - 50);
		start = new GuiElementButton(25, custom.y + custom.height + 10, Display.getWidth() - 50, "Connect to Custom");

		local.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				Main.instance.gui = new GuiLocalGames();
				/*
				 * Main.instance.objects.clear(); Main.instance.gui = new
				 * GuiConnecting(); new Thread(new Runnable() {
				 * 
				 * @Override public void run() {
				 * Main.instance.connectToServer("127.0.0.1"); } }).start();
				 */
			}
		};

		start.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				Main.instance.gui = null;
				Main.instance.objects.clear();
				Main.instance.gui = new GuiConnecting();
				new Thread(new Runnable()
				{
					@Override
					public void run()
					{
						Main.instance.connectToServer(custom.text);
					}
				}).start();
			}
		};

		back = new GuiElementButton(25, Display.getHeight() - (start.height + 10), Display.getWidth() - 50, "Cancel");
		back.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				Main.instance.gui = new GuiMainMenu();
			}
		};
	}

	@Override
	public void mouse(int key, boolean state)
	{
		custom.mouse(key, state);
	}

}
