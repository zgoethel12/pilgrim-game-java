package net.jibini.pilgrim.gui;

import java.net.InetAddress;
import java.util.concurrent.ConcurrentHashMap;
import net.jibini.networking.udp.Listener;
import net.jibini.pilgrim.Main;
import org.lwjgl.opengl.Display;

/**
 *
 * @author zgoethel12
 */
public class GuiLocalGames extends Gui
{

	public ConcurrentHashMap<String, GuiElementButton> games = new ConcurrentHashMap<String, GuiElementButton>();
	public GuiElementButton back;
	boolean multiplayer = true, isServer = false;
	public Listener l;

	public GuiLocalGames()
	{
		controls();
		startListener();
	}

	@Override
	public void render()
	{
		for (GuiElementButton slot : games.values())
		{
			slot.render();
		}

		back.render();

		if (Display.wasResized())
		{
			controls();
		}
	}

	public final void controls()
	{
		for (int i = 0; i < games.size(); i++)
		{
			GuiElementButton geb = games.values().toArray(new GuiElementButton[0])[i];
			geb.x = 25;
			geb.y = (geb.height + 10) * i + 25;
			geb.width = Display.getWidth() - 50;
		}

		back = new GuiElementButton(25, 0, Display.getWidth() - 50, "Cancel");
		back.y = Display.getHeight() - (back.height + 10);
		back.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				Main.instance.gui = new GuiMainMenu();
			}
		};
	}

	public final void startListener()
	{
		l = new Listener(new Listener.OnFound()
		{
			@Override
			public void onFound(InetAddress inet, net.jibini.networking.packets.Packet beat)
			{
				final String name = inet.getHostName();
				if (!games.contains(name))
				{
					net.jibini.pilgrim.networking.packets.PacketServerInfo psi = (net.jibini.pilgrim.networking.packets.PacketServerInfo) beat;
					GuiElementButton geb = new GuiElementButton(0, 0, 0, "Join " + psi.user + "'s Local Game");
					geb.onClick = new OnClickListener()
					{
						@Override
						public void onClick()
						{
							Main.instance.objects.clear();
							Main.instance.gui = new GuiConnecting();
							new Thread(new Runnable()
							{
								@Override
								public void run()
								{
									Main.instance.connectToServer(name);
								}
							}).start();
						}
					};
					games.put(name, geb);
					controls();
				}
			}
		});
	}
}
