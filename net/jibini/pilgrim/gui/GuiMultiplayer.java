package net.jibini.pilgrim.gui;

import net.jibini.pilgrim.Main;
import org.lwjgl.opengl.Display;

/**
 *
 * @author zgoethel12
 */
public class GuiMultiplayer extends Gui
{

	public GuiElementButton server, client, back;

	public GuiMultiplayer()
	{
		controls();
	}

	@Override
	public void render()
	{
		server.render();
		client.render();
		back.render();

		if (Display.wasResized())
		{
			controls();
		}
	}

	public final void controls()
	{
		server = new GuiElementButton(25, 10, Display.getWidth() - 50, "Start Server (Port 25566)");
		client = new GuiElementButton(25, 55, Display.getWidth() - 50, "Connect To Server");
		back = new GuiElementButton(25, Display.getHeight() - (client.height + 10), Display.getWidth() - 50, "Cancel");

		server.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				Main.instance.gui = null;
				Main.instance.startServer();
			}
		};

		client.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				Main.instance.gui = null;
				Main.instance.objects.clear();
				Main.instance.gui = new GuiConnecting();
				new Thread(new Runnable()
				{
					@Override
					public void run()
					{
						Main.instance.gui = new GuiJoinServer();
					}
				}).start();
			}
		};

		back.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				Main.instance.gui = new GuiMainMenu();
			}
		};
	}

}
