package net.jibini.pilgrim.gui;

import net.jibini.pilgrim.Main;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.opengl.TextureImpl;

/**
 *
 * @author zgoethel12
 */
public class GuiConnecting extends Gui
{

	public GuiConnecting()
	{
		controls();
	}

	@Override
	public void render()
	{
		String msg = "Connecting to server...";
		float width = Main.instance.fontRenderer.getWidth(msg);
		float x = Display.getWidth() / 2 - width / 2;
		Main.instance.fontRenderer.drawString(x, Display.getHeight() / 2 - Main.instance.fontRenderer.getLineHeight(),
				msg);
		TextureImpl.bindNone();

		if (Display.wasResized())
		{
			controls();
		}
	}

	public final void controls()
	{

	}

}
