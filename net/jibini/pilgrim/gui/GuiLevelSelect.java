package net.jibini.pilgrim.gui;

import net.jibini.networking.server.Server;
import net.jibini.pilgrim.Main;
import net.jibini.pilgrim.networking.handlers.ServerHandler;
import net.jibini.pilgrim.networking.packets.PacketServerInfo;
import org.lwjgl.opengl.Display;

/**
 *
 * @author zgoethel12
 */
public class GuiLevelSelect extends Gui
{

	public GuiElementButton[] slots = new GuiElementButton[10];
	public GuiElementButton back;
	boolean multiplayer, isServer;

	public GuiLevelSelect(boolean multiplayer, boolean isServer)
	{
		this.multiplayer = multiplayer;
		this.isServer = isServer;
		controls();
	}

	@Override
	public void render()
	{
		for (GuiElementButton slot : slots)
		{
			slot.render();
		}

		back.render();

		if (Display.wasResized())
		{
			controls();
		}
	}

	public final void controls()
	{
		for (int i = 0; i < slots.length; i++)
		{
			final int fi = i;
			slots[i] = new GuiElementButton(25, 10 + 45 * i, Display.getWidth() - 50, "Slot " + (i + 1));
			slots[i].onClick = new OnClickListener()
			{
				@Override
				public void onClick()
				{
					Main.instance.startGame("slot" + fi, multiplayer, isServer);
					Main.instance.gui = null;
					if (multiplayer && isServer)
					{
						System.out.println("Starting game server.");
						String result = Server.start(25566, new ServerHandler(),
								new PacketServerInfo(Main.instance.settings.displayName), true);

						if (!result.equalsIgnoreCase("Good"))
						{
							Main.instance.game.isServer = true;
							Main.instance.game.multiplayer = false;
							Main.instance.exitGame();
							Main.instance.gui = new GuiDisplayMessage(result);
						}
					}

				}
			};
		}
		back = new GuiElementButton(25, Display.getHeight() - (slots[0].height + 10), Display.getWidth() - 50,
				"Cancel");
		back.onClick = new OnClickListener()
		{
			@Override
			public void onClick()
			{
				Main.instance.gui = new GuiMainMenu();
			}
		};
	}

}
