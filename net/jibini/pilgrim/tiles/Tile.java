package net.jibini.pilgrim.tiles;

import net.jibini.pilgrim.Main;
import net.jibini.pilgrim.assets.Textures;
import net.jibini.pilgrim.chunks.Chunk;
import net.jibini.pilgrim.materials.Material;
import net.jibini.platformer.objects.Box;
import net.jibini.platformer.objects.GameObject;
import net.jibini.platformer.objects.StaticBox;
import net.jibini.platformer.physics.MovementRestrictions;
import org.newdawn.slick.opengl.TextureImpl;
import org.lwjgl.util.vector.Vector2f;

/**
 *
 * @author zgoethel12
 */
import static org.lwjgl.opengl.GL11.*;

public class Tile
{
	public static final float TILE_SIZE = 50;
	public static boolean initialized = false;

	public static void initialize()
	{
		if (initialized)
		{
			System.out.println("Tiles already initialized.");
		} else
		{
			System.out.println("Initializing tiles...");
			initialized = true;
		}
	}

	public static void renderTile(float x, float y, Vector2f coords)
	{
		TextureImpl.bindNone();
		Textures.TILES.bind();
		float uniform = 1f / 32;

		glBegin(GL_QUADS);
		glTexCoord2f(coords.x * uniform + uniform, coords.y * uniform);
		glVertex2f(x * TILE_SIZE + TILE_SIZE, y * TILE_SIZE + TILE_SIZE);
		glTexCoord2f(coords.x * uniform, coords.y * uniform);
		glVertex2f(x * TILE_SIZE, y * TILE_SIZE + TILE_SIZE);
		glTexCoord2f(coords.x * uniform, coords.y * uniform + uniform);
		glVertex2f(x * TILE_SIZE, y * TILE_SIZE);
		glTexCoord2f(coords.x * uniform + uniform, coords.y * uniform + uniform);
		glVertex2f(x * TILE_SIZE + TILE_SIZE, y * TILE_SIZE);
		glEnd();

		TextureImpl.bindNone();
	}

	private Material type;
	public int x, y;
	public double strength = 0;
	public int parent;
	public Box box;

	public Tile(Material type, int x, int y, int parent, Box box)
	{
		this.type = type;
		this.x = x;
		this.y = y;
		this.parent = parent;
		this.box = box;
	}

	public Material getType()
	{
		return type;
	}

	public void setType(Material type)
	{
		this.type = type;
		this.strength = type.getStrength();
		Chunk p = Main.instance.game.getChunk(parent);

		if (p != null)
		{
			p.built = false;
		}
	}

	public MovementRestrictions collide(GameObject go)
	{
		StaticBox sb = new StaticBox(x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
		MovementRestrictions mr = go.collides(sb);

		return getType() == Material.AIR ? new MovementRestrictions(false, false, false, false) : mr;
	}
}
