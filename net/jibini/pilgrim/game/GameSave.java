package net.jibini.pilgrim.game;

import java.io.Serializable;
import org.lwjgl.util.vector.Vector2f;

/**
 *
 * @author zgoethel12
 */
public class GameSave implements Serializable
{
	private static final long serialVersionUID = 6529685098267757690L;

	public String name;
	public int seed;
	public String character;
	public Vector2f location;

	public GameSave(String name, int seed, String character, Vector2f location)
	{
		this.name = name;
		this.seed = seed;
		this.character = character;
		this.location = location;
	}

	public GameSave()
	{
	}
}
