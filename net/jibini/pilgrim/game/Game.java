package net.jibini.pilgrim.game;

import net.jibini.pilgrim.chunks.ChunkLoader;
import net.jibini.pilgrim.chunks.Chunk;
import java.util.concurrent.CopyOnWriteArrayList;
import net.jibini.networking.client.Client;
import net.jibini.networking.server.Server;
import net.jibini.pilgrim.Main;
import net.jibini.pilgrim.Message;
import net.jibini.pilgrim.assets.Textures;
import net.jibini.pilgrim.entity.Entity;
import net.jibini.pilgrim.objects.Updater;
import net.jibini.pilgrim.humans.EntityHuman;
import net.jibini.pilgrim.humans.EntityHumanMP;
import net.jibini.pilgrim.gui.GuiDisplayMessage;
import net.jibini.pilgrim.inventory.Inventory;
import net.jibini.pilgrim.materials.Material;
import net.jibini.pilgrim.networking.packets.PacketClientChunkRequest;
import net.jibini.pilgrim.networking.packets.PacketTileUpdate;
import net.jibini.pilgrim.objects.Cloud;
import net.jibini.pilgrim.tiles.Tile;
import net.jibini.platformer.SubLoop;
import net.jibini.platformer.objects.Box;
import net.jibini.platformer.objects.GameObject;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.opengl.TextureImpl;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author zgoethel12
 */
import static org.lwjgl.opengl.GL11.*;

public class Game extends SubLoop
{
	public String name;
	public int seed;
	public EntityHuman player;

	public CopyOnWriteArrayList<Chunk> chunks = new CopyOnWriteArrayList<Chunk>();
	public CopyOnWriteArrayList<EntityHumanMP> otherPlayers = new CopyOnWriteArrayList<EntityHumanMP>();
	public CopyOnWriteArrayList<Runnable> delayedRender = new CopyOnWriteArrayList<Runnable>();
	public CopyOnWriteArrayList<Message> messages = new CopyOnWriteArrayList<Message>();
	public ChunkLoader loader;
	public float time = 30000;
	public int maxTime = 100000;
	public float maxZoom = 1.25f;
	public float minZoom = 0.25f;
	public float zoom = 1;
	public boolean multiplayer;
	public boolean isServer;
	public int clOverride = 0, crOverride = 0;
	public boolean calcLocation = false;

	public Game(String name, int seed, boolean multiplayer, boolean isServer, Inventory inventory, Vector2f location)
	{
		this.name = name;
		this.seed = seed;
		this.player = new EntityHuman(Main.instance.settings.character, 0, 4000);
		this.player.inventory = inventory;
		this.multiplayer = multiplayer;
		this.isServer = isServer;

		if (location != null)
		{
			this.player.x = location.x;
			this.player.y = location.y;
		} else
		{
			calcLocation = true;
		}
	}

	@Override
	public void initGL()
	{

	}

	@Override
	public void init()
	{
		if (isServer)
		{
			loader = new ChunkLoader(name);
		}

		Main.instance.objects.add(player);
		Main.instance.objects.add(new Cloud());

		if (isServer)
		{
			addChunk(loader.loadChunk(0));
		}
	}

	@Override
	public void update()
	{
		try
		{
			Updater.update();

			if (multiplayer && !isServer)
			{
				if (!Client.connection.connected)
				{
					Main.instance.exitGame();
					Main.instance.gui = new GuiDisplayMessage("Lost connection to the server.");
				}
			}

			Chunk leftChunk = null;
			Chunk rightChunk = null;

			for (Chunk c : chunks)
			{
				if (leftChunk == null || rightChunk == null)
				{
					leftChunk = c;
					rightChunk = c;
				}

				if (c.position > rightChunk.position)
				{
					rightChunk = c;
				}

				if (c.position < leftChunk.position)
				{
					leftChunk = c;
				}
			}

			float rightChunkLeft = rightChunk == null ? (-1)
					: (rightChunk.position * Chunk.WIDTH * Tile.TILE_SIZE - player.x);
			float leftChunkRight = leftChunk == null ? (-1)
					: ((leftChunk.position + 1) * Chunk.WIDTH * Tile.TILE_SIZE - player.x);
			float rightChunkRight = rightChunk == null ? (-1)
					: ((rightChunk.position + 1) * Chunk.WIDTH * Tile.TILE_SIZE - player.x);
			float leftChunkLeft = leftChunk == null ? (-1)
					: (leftChunk.position * Chunk.WIDTH * Tile.TILE_SIZE - player.x);

			final Chunk rc = rightChunk, lc = leftChunk;

			if (rightChunkLeft > Display.getWidth() / 2 / zoom)
			{
				removeChunk(rightChunk);

				new Thread(new Runnable()
				{
					@Override
					public void run()
					{
						if (isServer)
						{
							loader.saveChunk(rc);
						}

						crOverride--;
					}
				}).start();
			}

			if (leftChunkRight < -Display.getWidth() / 2 / zoom)
			{
				removeChunk(leftChunk);

				new Thread(new Runnable()
				{
					@Override
					public void run()
					{
						if (isServer)
						{
							loader.saveChunk(lc);
						}

						clOverride++;
					}
				}).start();
			}

			if (rightChunkRight < Display.getWidth() / 2 / zoom)
			{
				if (crOverride < (rc == null ? (-1) : (rc.position + 1)))
				{
					new Thread(new Runnable()
					{
						@Override
						public void run()
						{
							if (isServer)
							{
								Chunk adding = loader.loadChunk(rc.position + 1);
								addChunk(adding);
								Chunk left = getChunk(adding.position - 1);

								if (left != null)
								{
									left.built = false;
								}
							} else
							{
								Client.sendPacket(new PacketClientChunkRequest(rc.position + 1));
							}
						}
					}).start();

					crOverride = rc.position + 1;
				}
			}
			if (leftChunkLeft > -Display.getWidth() / 2 / zoom)
			{
				if (clOverride > (lc == null ? (-1) : (lc.position - 1)))
				{
					new Thread(new Runnable()
					{
						@Override
						public void run()
						{
							try
							{
								if (isServer)
								{
									Chunk adding = loader.loadChunk(lc.position - 1);
									addChunk(adding);
									Chunk right = getChunk(adding.position + 1);

									if (right != null)
									{
										right.built = false;
									}
								} else
								{
									Client.sendPacket(new PacketClientChunkRequest(lc.position - 1));
								}
							} catch (NullPointerException ex)
							{

							}
						}
					}).start();

					clOverride = lc.position - 1;
				}
			}

			zoom += (float) Mouse.getDWheel() / 500;

			if (zoom < minZoom)
			{
				zoom = minZoom;
			} else if (zoom > maxZoom)
			{
				zoom = maxZoom;
			}

			for (Message m : messages)
			{
				m.update();
			}

			if (calcLocation)
			{
				if (chunks.get(0) != null)
				{
					for (int i = Chunk.HEIGHT - 1; i >= 0; i--)
					{
						if (chunks.get(0).tiles[0][i].getType().getID() != Material.AIR.getID())
						{
							player.x = 0;
							player.y = (i + 1) * Tile.TILE_SIZE;
							System.out.println(player.y);

							calcLocation = false;
							break;
						}
					}
				}
			}
		} catch (Exception ex)
		{
		}
	}

	public void handleMouse(int button, boolean state)
	{
		Vector2f selection = getSelectedVector();
		int x = (int) (selection.x / Tile.TILE_SIZE);
		int y = (int) (selection.y / Tile.TILE_SIZE);
		Tile selected = getTileAt(x, y);

		if (button == 2 && state && canPlace(x, y))
		{
			selected.setType(Material.BRICK);
		} else if (button == 1 && state && canPlace(x, y))
		{
			selected.setType(Material.DIRT);
		}

		if (multiplayer && isServer && state && button > 0)
		{
			Server.globalPacket(
					new PacketTileUpdate(selected.parent, selected.x, selected.y, selected.getType().getID()), true);
		} else if (multiplayer && !isServer && state && button > 0)
		{
			Client.sendPacket(
					new PacketTileUpdate(selected.parent, selected.x, selected.y, selected.getType().getID()));
		}
	}

	public EntityHumanMP getHumanFromUUID(int uuid)
	{
		EntityHumanMP eh = null;

		for (EntityHumanMP ehmp : otherPlayers)
		{
			if (ehmp.uuid == uuid)
			{
				eh = ehmp;
			}
		}

		return eh;
	}

	public void addPlayer(EntityHumanMP p)
	{
		otherPlayers.add(p);
		Main.instance.objects.add(p);
	}

	public void removePlayer(EntityHumanMP p)
	{
		otherPlayers.remove(p);
		Main.instance.objects.remove(p);
	}

	@Override
	public void render()
	{
		gui();
		drawSkyGradient();

		game();
		float z = zoom;
		glScalef(z, z, z);

		Vector3f t = new Vector3f(0 - player.x, 0 - (player.y + player.getBoundingBox().h / 2), 0);
		glTranslatef(t.x, t.y, t.z);

		for (GameObject go : Main.instance.objects)
		{
			if (!(go instanceof Box))
			{
				try
				{
					go.render();
				} catch (Exception ex)
				{
				}
			}
		}

		if (Main.instance.gui == null)
		{
			drawHelperBox();
		}

		for (Runnable r : delayedRender)
		{
			r.run();
			delayedRender.remove(r);
		}

		glPushMatrix();
		gui();
		glTranslatef(0, 0, 800);

		if (Main.instance.settings.vignette)
		{
			drawVignette();
		}

		glTranslatef(0, 0, 50);

		try
		{
			if (Keyboard.isKeyDown(Keyboard.KEY_F3))
			{
				Main.instance.fontRenderer.drawString(10, 110, "Frames per second: " + Main.instance.fps);
				Main.instance.fontRenderer.drawString(10, 145, "Chunks: " + chunks.size());
				TextureImpl.bindNone();
			}

			for (int i = 0; i < messages.size(); i++)
			{
				Message m = messages.get(i);
				int p = messages.size() - i - 2;
				int length = Main.instance.fontRenderer.getWidth(m.msg);
				int x = Display.getWidth() - (length + 10);
				int y = Display.getHeight() - (p * (Main.instance.fontRenderer.getHeight() + 2) + 100);
				Main.instance.fontRenderer.drawString(x, y, m.msg);
			}

			TextureImpl.bindNone();
		} catch (Exception ex)
		{
		}

		glTranslatef(0, 0, 1);
		player.renderHotbar();

		glPopMatrix();
	}

	public Chunk getChunk(int position)
	{
		for (Chunk c : chunks)
		{
			if (c.position == position)
			{
				return c;
			}
		}

		return null;
	}

	public Entity getEntityById(int id)
	{
		for (GameObject go : Main.instance.objects)
		{
			if (go instanceof Entity)
			{
				Entity e = (Entity) go;

				if (e.getID() == id)
				{
					return e;
				}
			}
		}

		return null;
	}

	@Override
	public void destroy()
	{

	}

	@Override
	public void destroyGL()
	{
		for (Chunk c : chunks)
		{
			c.destroyList();
		}
	}

	public Vector2f getSelectedVector()
	{
		float mx = Mouse.getX() - Display.getWidth() / 2;
		float my = Mouse.getY() - Display.getHeight() / 2;
		float wx = (mx / Main.instance.game.zoom + Main.instance.game.player.x);
		float wy = (my / Main.instance.game.zoom + Main.instance.game.player.y) - 25;
		float boxX = wx - wx % Tile.TILE_SIZE;
		float boxY = wy - wy % Tile.TILE_SIZE;

		if (wx < 0)
		{
			boxX -= Tile.TILE_SIZE;
		}

		boxY += Tile.TILE_SIZE * 2;
		return new Vector2f(boxX, boxY);
	}

	public Tile getTileAt(int x, int y)
	{
		try
		{
			Chunk chunk = getChunk((x / Chunk.WIDTH) - (x < 0 ? 1 : 0));

			if (x < 0 && x % 32 == 0)
			{
				chunk = getChunk((x / Chunk.WIDTH));
				x = 0;
			}

			if (chunk == null)
			{
				return null;
			} else
			{
				int mx = Math.abs(x) % Chunk.WIDTH;

				if (x < 0)
				{
					mx = Chunk.WIDTH - mx;
				}

				return chunk.tiles[mx - (x < 0 && x % Chunk.WIDTH == 0 ? 1 : 0)][y];
			}
		} catch (Exception ex)
		{
			return null;
		}
	}

	public void drawHelperBox()
	{
		Vector2f box = getSelectedVector();
		int x = (int) (box.x / Tile.TILE_SIZE);
		int y = (int) (box.y / Tile.TILE_SIZE);
		Textures.HELPER_BOX.bind();

		if (canBreak(x, y))
		{
			Tile s = getTileAt(x, y);
			float c = 1 - (float) s.strength / s.getType().getStrength();
			glColor3f(c, c, c);
		} else if (canPlace(x, y))
		{
			glColor3f(0, 1, 0);
		} else if (isInRange(x, y))
		{
			glColor3f(1, 1, 0);
		} else
		{
			glColor3f(1, 0, 0);
		}

		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex3f(box.x, box.y, 10);
		glTexCoord2f(1, 0);
		glVertex3f(box.x + Tile.TILE_SIZE, box.y, 10);
		glTexCoord2f(1, 1);
		glVertex3f(box.x + Tile.TILE_SIZE, box.y + Tile.TILE_SIZE, 10);
		glTexCoord2f(0, 1);
		glVertex3f(box.x, box.y + Tile.TILE_SIZE, 10);
		glEnd();
		glColor3f(1, 1, 1);
		TextureImpl.bindNone();

		if (isInRange(x, y))
		{
			float x1 = Main.instance.game.player.x;
			float y1 = Main.instance.game.player.y + 75;
			float x2 = box.x + 25;
			float y2 = box.y + 25;
			glLineWidth(2);
			glBegin(GL_LINES);
			glVertex3f(x1, y1, 3);
			glVertex3f(x2, y2, 3);
			glEnd();
			glLineWidth(1);
		}
	}

	public boolean canPlace(int x, int y)
	{
		boolean canPlace = false;
		Tile left = getTileAt(x - 1, y);
		Tile right = getTileAt(x + 1, y);
		Tile bottom = getTileAt(x, y - 1);
		Tile top = getTileAt(x, y + 1);

		if (left != null)
		{
			if (left.getType() != Material.AIR)
			{
				canPlace = true;
			}
		}

		if (right != null)
		{
			if (right.getType() != Material.AIR)
			{
				canPlace = true;
			}
		}

		if (bottom != null)
		{
			if (bottom.getType() != Material.AIR)
			{
				canPlace = true;
			}
		}

		if (top != null)
		{
			if (top.getType() != Material.AIR)
			{
				canPlace = true;
			}
		}

		return canPlace && !canBreak(x, y) && isInRange(x, y);
	}

	public boolean canBreak(int x, int y)
	{
		Tile self = getTileAt(x, y);
		boolean canBreak = false;

		if (self != null)
		{
			if (self.getType() != Material.AIR)
			{
				canBreak = true;
			}
		}

		return canBreak && isInRange(x, y);
	}

	public void addChunk(Chunk c)
	{
		chunks.add(c);
		Main.instance.objects.add(c);
	}

	public void removeChunk(Chunk c)
	{
		chunks.remove(c);
		Main.instance.objects.remove(c);
		c.destroyList();
	}

	public boolean isInRange(float x, float y)
	{
		return (Main.instance.distance(new Vector2f(x, y), new Vector2f(Main.instance.game.player.x / Tile.TILE_SIZE,
				(Main.instance.game.player.y + 75) / Tile.TILE_SIZE)) <= 8);
	}

	public void drawSkyGradient()
	{
		Textures.SKY_GRADIENT.bind();
		float gradTop = 0.6f;
		float gradBottom = 1;
		glBegin(GL_QUADS);
		glTexCoord2f((float) time / maxTime, 0);
		glColor3f(gradBottom, gradBottom, gradBottom);
		glVertex3f(0, Display.getHeight(), -100);
		glVertex3f(Display.getWidth(), Display.getHeight(), -100);
		glColor3f(gradTop, gradTop, gradTop);
		glVertex3f(Display.getWidth(), 0, -100);
		glVertex3f(0, 0, -100);
		glEnd();
		glColor3f(1, 1, 1);
		TextureImpl.bindNone();
	}

	public void drawVignette()
	{
		glColor4f(1, 1, 1, 0.25f);
		Textures.VIGNETTE.bind();
		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2f(0, Display.getHeight());
		glTexCoord2f(1, 0);
		glVertex2f(Display.getWidth(), Display.getHeight());
		glTexCoord2f(1, 1);
		glVertex2f(Display.getWidth(), 0);
		glTexCoord2f(0, 1);
		glVertex2f(0, 0);
		glEnd();
		TextureImpl.bindNone();
		glColor4f(1, 1, 1, 1);
	}
}
