package net.jibini.pilgrim.game;

import java.io.File;
import net.jibini.pilgrim.chunks.Chunk;
import net.jibini.pilgrim.inventory.Inventory;
import net.jibini.pilgrim.utilities.FileHelper;
import org.lwjgl.util.vector.Vector2f;

/**
 *
 * @author zgoethel12
 */
public class GameLoader
{
	public static Game loadGame(String name, boolean multiplayer)
	{
		System.out.println("Loading game: " + name);

		char sc = File.separatorChar;
		Object loaded = FileHelper.loadObject("saves" + sc + name + sc + "game.dat");
		Object inv = FileHelper.loadObject("saves" + sc + name + sc + "inventory.dat");

		if (loaded == null)
		{
			return null;
		} else
		{
			GameSave gs = (GameSave) loaded;
			Game game;

			if (inv == null)
				game = new Game(gs.name, gs.seed, multiplayer, true, new Inventory("Player Inventory", 30),
						gs.location);
			else
				game = new Game(gs.name, gs.seed, multiplayer, true, (Inventory) inv, gs.location);

			return game;
		}
	}

	public static void saveGame(Game game, boolean saveChunks)
	{
		System.out.println("Saving game: " + game.name);
		char sc = File.separatorChar;

		if (saveChunks)
		{
			for (Chunk chunk : game.chunks)
			{
				game.loader.saveChunk(chunk);
			}
		}

		GameSave gs = new GameSave();
		gs.name = game.name;
		gs.seed = game.seed;
		gs.character = game.player.name;
		gs.location = new Vector2f(game.player.x, game.player.y);

		FileHelper.saveObject(gs, "saves" + sc + game.name + sc + "game.dat");
		FileHelper.saveObject(game.player.inventory, "saves" + sc + game.name + sc + "inventory.dat");
	}
}
