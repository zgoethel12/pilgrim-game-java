package net.jibini.pilgrim.networking.handlers;

import net.jibini.networking.Connection;
import net.jibini.networking.packets.Packet;
import net.jibini.networking.packets.PacketHandler;
import net.jibini.pilgrim.humans.HumanSerializable;
import net.jibini.pilgrim.Main;
import net.jibini.pilgrim.Message;
import net.jibini.pilgrim.chunks.Chunk;
import net.jibini.pilgrim.chunks.ChunkSave;
import net.jibini.pilgrim.entity.EntityDrop;
import net.jibini.pilgrim.humans.EntityHuman;
import net.jibini.pilgrim.humans.EntityHumanMP;
import net.jibini.pilgrim.materials.Material;
import net.jibini.pilgrim.networking.packets.PacketDrop;
import net.jibini.pilgrim.networking.packets.PacketKillDrop;
import net.jibini.pilgrim.networking.packets.PacketMessage;
import net.jibini.pilgrim.networking.packets.PacketParticle;
import net.jibini.pilgrim.networking.packets.PacketServerAnnouncement;
import net.jibini.pilgrim.networking.packets.PacketServerChunk;
import net.jibini.pilgrim.networking.packets.PacketServerPlayerDisconnect;
import net.jibini.pilgrim.networking.packets.PacketServerPlayersUpdate;
import net.jibini.pilgrim.networking.packets.PacketTileUpdate;
import net.jibini.pilgrim.objects.Particle;
import net.jibini.platformer.objects.GameObject;

/**
 *
 * @author zgoethel12
 */
public class ClientHandler extends PacketHandler
{

	@Override
	public void handlePacket(Packet packet, Connection connection)
	{
		if (packet instanceof PacketServerAnnouncement)
		{
			PacketServerAnnouncement psa = (PacketServerAnnouncement) packet;
			System.out.println(psa.payload);
		} else if (packet instanceof PacketServerChunk)
		{
			PacketServerChunk psc = (PacketServerChunk) packet;
			ChunkSave cs = psc.chunk;
			Chunk c = new Chunk();
			c.loadSave(cs, false);
			Main.instance.game.addChunk(c);
			Chunk right = Main.instance.game.getChunk(c.position + 1);
			Chunk left = Main.instance.game.getChunk(c.position - 1);
			if (right != null)
			{
				right.built = false;
			}
			if (left != null)
			{
				left.built = false;
			}
		} else if (packet instanceof PacketServerPlayersUpdate)
		{
			PacketServerPlayersUpdate pspu = (PacketServerPlayersUpdate) packet;
			for (final HumanSerializable hs : pspu.humans)
			{
				if (hs.uuid != Main.instance.game.player.uuid)
				{
					EntityHumanMP ehmp = Main.instance.game.getHumanFromUUID(hs.uuid);

					if (ehmp == null)
					{
						if (!Main.instance.overriddenUUIDs.contains(hs.uuid))
						{
							Main.instance.overriddenUUIDs.add(hs.uuid);
							Main.instance.mainThread.add(new Runnable()
							{
								@Override
								public void run()
								{
									EntityHuman eh = EntityHumanMP.loadSerializable(hs);
									eh.uuid = hs.uuid;
									Main.instance.game.otherPlayers.add(0, (EntityHumanMP) eh);
									Main.instance.objects.add(0, eh);
									Main.instance.overriddenUUIDs.remove((Integer) eh.uuid);

									Main.instance.game.messages
											.add(new Message(((EntityHumanMP) eh).displayName + " joined the game."));
								}
							});
						}
					} else
					{
						ehmp.x = hs.x;
						ehmp.y = hs.y;
						ehmp.ca.current = hs.anim;
						ehmp.ca.current.direction = hs.direction;
						ehmp.ca.current.stage = hs.stage;
					}
				}
			}
		} else if (packet instanceof PacketServerPlayerDisconnect)
		{
			PacketServerPlayerDisconnect pspd = (PacketServerPlayerDisconnect) packet;
			EntityHumanMP ehmp = Main.instance.game.getHumanFromUUID(pspd.uuid);

			if (ehmp != null)
			{
				Main.instance.game.removePlayer(ehmp);

				Main.instance.game.messages.add(new Message(ehmp.displayName + " left the game."));
			}
		} else if (packet instanceof PacketTileUpdate)
		{
			PacketTileUpdate ptu = (PacketTileUpdate) packet;
			Chunk affected = Main.instance.game.getChunk(ptu.chunk);
			if (affected != null)
			{
				Main.instance.game.getTileAt(ptu.x, ptu.y).setType(Material.list.get(ptu.id));
			}
		} else if (packet instanceof PacketParticle)
		{
			PacketParticle pp = (PacketParticle) packet;
			Particle p = new Particle(pp.r, pp.g, pp.b, pp.x, pp.y, pp.vx, pp.vy);
			p.noCollide = pp.noCollide;
			if (!Main.instance.settings.reducedParticles || (Main.instance.rand.nextInt(4) == 0))
			{
				if (Main.instance.settings.particleNoClip)
				{
					p.noCollide = true;
				}
				Main.instance.objects.add(p);
			}
		} else if (packet instanceof PacketMessage)
		{
			PacketMessage pm = (PacketMessage) packet;
			Main.instance.game.messages.add(new Message(pm.user + ": " + pm.msg));
		} else if (packet instanceof PacketDrop)
		{
			PacketDrop pd = (PacketDrop) packet;
			EntityDrop ed = new EntityDrop(pd.x, pd.y, pd.vx, pd.vy, Material.list.get(pd.material), pd.tracker);
			Main.instance.objects.add(ed);
		} else if (packet instanceof PacketKillDrop)
		{
			PacketKillDrop pkd = (PacketKillDrop) packet;
			EntityDrop kill = null;
			for (GameObject go : Main.instance.objects)
			{
				if (go instanceof EntityDrop)
				{
					EntityDrop ed = (EntityDrop) go;
					if (ed.tracker == pkd.tracker)
					{
						kill = ed;
					}
				}
			}
			if (Main.instance.objects.contains(kill))
			{
				Main.instance.objects.remove(kill);
			}
		}
	}

}
