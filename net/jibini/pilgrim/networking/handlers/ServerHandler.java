package net.jibini.pilgrim.networking.handlers;

import net.jibini.networking.Connection;
import net.jibini.networking.packets.Packet;
import net.jibini.networking.packets.PacketHandler;
import net.jibini.networking.server.Server;
import net.jibini.pilgrim.Main;
import net.jibini.pilgrim.Message;
import net.jibini.pilgrim.chunks.Chunk;
import net.jibini.pilgrim.entity.EntityDrop;
import net.jibini.pilgrim.humans.EntityHumanMP;
import net.jibini.pilgrim.materials.Material;
import net.jibini.pilgrim.networking.packets.PacketClientChunkRequest;
import net.jibini.pilgrim.networking.packets.PacketClientConnect;
import net.jibini.pilgrim.networking.packets.PacketClientPlayerUpdate;
import net.jibini.pilgrim.networking.packets.PacketClientPoll;
import net.jibini.pilgrim.networking.packets.PacketDrop;
import net.jibini.pilgrim.networking.packets.PacketKillDrop;
import net.jibini.pilgrim.networking.packets.PacketMessage;
import net.jibini.pilgrim.networking.packets.PacketParticle;
import net.jibini.pilgrim.networking.packets.PacketServerAnnouncement;
import net.jibini.pilgrim.networking.packets.PacketServerChunk;
import net.jibini.pilgrim.networking.packets.PacketTileUpdate;
import net.jibini.pilgrim.objects.Particle;
import net.jibini.platformer.objects.GameObject;

/**
 *
 * @author zgoethel12
 */
public class ServerHandler extends PacketHandler
{

	@Override
	public void handlePacket(Packet packet, Connection connection)
	{
		final Packet fp = packet;
		final Connection fc = connection;
		if (packet instanceof PacketClientPoll)
		{
			connection.sendQueue.add(new PacketServerAnnouncement("Hello, client!"));
		} else if (packet instanceof PacketClientChunkRequest)
		{
			new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					PacketClientChunkRequest pccr = (PacketClientChunkRequest) fp;
					Chunk existing = Main.instance.game.getChunk(pccr.position);
					if (existing == null)
					{
						existing = Main.instance.game.loader.loadChunk(pccr.position);
					}
					fc.sendQueue.add(new PacketServerChunk(existing.getSave()));
				}
			}).start();
		} else if (packet instanceof PacketClientConnect)
		{
			Main.instance.mainThread.add(new Runnable()
			{
				@Override
				public void run()
				{
					PacketClientConnect pcc = (PacketClientConnect) fp;
					EntityHumanMP ehmp = new EntityHumanMP(pcc.name, pcc.character, 0, 0, fc);
					ehmp.uuid = pcc.uuid;
					Main.instance.game.otherPlayers.add(0, ehmp);
					Main.instance.objects.add(0, ehmp);

					Main.instance.game.messages.add(new Message(ehmp.displayName + " joined the game."));
				}
			});
		} else if (packet instanceof PacketClientPlayerUpdate)
		{
			PacketClientPlayerUpdate pcpu = (PacketClientPlayerUpdate) packet;
			EntityHumanMP ehmp = null;
			for (EntityHumanMP op : Main.instance.game.otherPlayers)
			{
				if (op.connection == connection)
				{
					ehmp = op;
				}
			}
			if (ehmp != null)
			{
				ehmp.x = pcpu.x;
				ehmp.y = pcpu.y;
				ehmp.ca.current = pcpu.anim;
				ehmp.ca.current.direction = pcpu.direction;
				ehmp.ca.current.stage = pcpu.stage;
				ehmp.uuid = pcpu.uuid;
			}
		} else if (packet instanceof PacketTileUpdate)
		{
			final PacketTileUpdate ptu = (PacketTileUpdate) packet;
			Server.globalPacket(ptu, true);
			Chunk affected = Main.instance.game.getChunk(ptu.chunk);
			if (affected != null)
			{
				Main.instance.game.getTileAt(ptu.x, ptu.y).setType(Material.list.get(ptu.id));
			} else
			{
				new Thread(new Runnable()
				{
					@Override
					public void run()
					{
						Chunk c = Main.instance.game.loader.loadChunk(ptu.chunk);
						Main.instance.game.addChunk(c);
						Main.instance.game.getTileAt(ptu.x, ptu.y).setType(Material.list.get(ptu.id));
						Main.instance.game.loader.saveChunk(c);
						Main.instance.game.removeChunk(c);
					}
				}).start();
			}
		} else if (packet instanceof PacketParticle)
		{
			PacketParticle pp = (PacketParticle) packet;
			Particle p = new Particle(pp.r, pp.g, pp.b, pp.x, pp.y, pp.vx, pp.vy);
			p.noCollide = pp.noCollide;
			Server.globalPacket(packet, true);
			if (!Main.instance.settings.reducedParticles || (Main.instance.rand.nextInt(4) == 0))
			{
				if (Main.instance.settings.particleNoClip)
				{
					p.noCollide = true;
				}
				Main.instance.objects.add(p);
			}
		} else if (packet instanceof PacketMessage)
		{
			Server.globalPacket(packet, true);
			PacketMessage pm = (PacketMessage) packet;
			Main.instance.game.messages.add(new Message(pm.user + ": " + pm.msg));
		} else if (packet instanceof PacketKillDrop)
		{
			PacketKillDrop pkd = (PacketKillDrop) packet;
			EntityDrop kill = null;
			for (GameObject go : Main.instance.objects)
			{
				if (go instanceof EntityDrop)
				{
					EntityDrop ed = (EntityDrop) go;
					if (ed.tracker == pkd.tracker)
					{
						kill = ed;
					}
				}
			}
			if (Main.instance.objects.contains(kill))
			{
				Main.instance.objects.remove(kill);
			}
			Server.globalPacket(packet, true);
		} else if (packet instanceof PacketDrop)
		{
			PacketDrop pd = (PacketDrop) packet;
			EntityDrop ed = new EntityDrop(pd.x, pd.y, pd.vx, pd.vy, Material.list.get(pd.material), pd.tracker);
			Main.instance.objects.add(ed);
			Server.globalPacket(packet, true);
		}
	}

}
