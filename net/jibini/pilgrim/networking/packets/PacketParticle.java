package net.jibini.pilgrim.networking.packets;

import net.jibini.networking.packets.Packet;

/**
 *
 * @author zgoethel12
 */
public class PacketParticle extends Packet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public float r, g, b, x, y, vx, vy;
	public boolean noCollide;

	public PacketParticle(float r, float g, float b, float x, float y, float vx, float vy, boolean noCollide)
	{
		this.r = r;
		this.g = g;
		this.b = b;
		this.x = x;
		this.y = y;
		this.vx = vx;
		this.vy = vy;
		this.noCollide = noCollide;
	}

}
