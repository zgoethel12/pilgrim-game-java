package net.jibini.pilgrim.networking.packets;

import net.jibini.networking.packets.Packet;

/**
 *
 * @author zgoethel12
 */
public class PacketServerAnnouncement extends Packet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String payload;

	public PacketServerAnnouncement(String payload)
	{
		this.payload = payload;
	}

}
