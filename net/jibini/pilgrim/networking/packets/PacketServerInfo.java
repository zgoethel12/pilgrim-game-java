package net.jibini.pilgrim.networking.packets;

import net.jibini.networking.packets.Packet;

/**
 *
 * @author zgoethel12
 */
public class PacketServerInfo extends Packet
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String user;

	public PacketServerInfo(String user)
	{
		this.user = user;
	}
}
