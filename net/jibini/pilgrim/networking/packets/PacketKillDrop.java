package net.jibini.pilgrim.networking.packets;

import net.jibini.networking.packets.Packet;

/**
 *
 * @author zgoethel12
 */
public class PacketKillDrop extends Packet
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int tracker;

	public PacketKillDrop(int tracker)
	{
		this.tracker = tracker;
	}
}
