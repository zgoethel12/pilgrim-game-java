package net.jibini.pilgrim.networking.packets;

import net.jibini.networking.packets.Packet;
import net.jibini.pilgrim.humans.HumanSerializable;

/**
 *
 * @author zgoethel12
 */
public class PacketServerPlayersUpdate extends Packet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public HumanSerializable[] humans;

	public PacketServerPlayersUpdate(HumanSerializable[] humans)
	{
		this.humans = humans;
	}

}
