package net.jibini.pilgrim.networking.packets;

import net.jibini.networking.packets.Packet;

/**
 *
 * @author zgoethel12
 */
public class PacketClientChunkRequest extends Packet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int position;

	public PacketClientChunkRequest(int position)
	{
		this.position = position;
	}

}
