package net.jibini.pilgrim.networking.packets;

import net.jibini.networking.packets.Packet;
import net.jibini.pilgrim.chunks.ChunkSave;

/**
 *
 * @author zgoethel12
 */
public class PacketServerChunk extends Packet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ChunkSave chunk;

	public PacketServerChunk(ChunkSave chunk)
	{
		this.chunk = chunk;
	}

}
