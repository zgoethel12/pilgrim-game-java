package net.jibini.pilgrim.networking.packets;

import net.jibini.networking.packets.Packet;

/**
 *
 * @author zgoethel12
 */
public class PacketTileUpdate extends Packet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int chunk;
	public int x;
	public int y;
	public int id;

	public PacketTileUpdate(int chunk, int x, int y, int id)
	{
		this.chunk = chunk;
		this.x = x;
		this.y = y;
		this.id = id;
	}

}
