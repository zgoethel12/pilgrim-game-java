package net.jibini.pilgrim.networking.packets;

import net.jibini.networking.packets.Packet;
import net.jibini.platformer.character.CharacterAnimation;

/**
 *
 * @author zgoethel12
 */
public class PacketClientPlayerUpdate extends Packet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public float x, y;
	public CharacterAnimation anim;
	public int direction, stage, uuid;

	public PacketClientPlayerUpdate(float x, float y, CharacterAnimation anim, int direction, int stage, int uuid)
	{
		this.x = x;
		this.y = y;
		this.anim = anim;
		this.direction = direction;
		this.stage = stage;
		this.uuid = uuid;
	}

}
