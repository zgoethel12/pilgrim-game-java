package net.jibini.pilgrim.networking.packets;

import net.jibini.networking.packets.Packet;

/**
 *
 * @author zgoethel12
 */
public class PacketServerPlayerDisconnect extends Packet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int uuid;

	public PacketServerPlayerDisconnect(int uuid)
	{
		this.uuid = uuid;
	}

}
