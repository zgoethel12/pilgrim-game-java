package net.jibini.pilgrim.networking.packets;

import net.jibini.networking.packets.Packet;

/**
 *
 * @author zgoethel12
 */
public class PacketMessage extends Packet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String user, msg;

	public PacketMessage(String user, String msg)
	{
		this.user = user;
		this.msg = msg;
	}

}
