package net.jibini.pilgrim.networking.packets;

import net.jibini.networking.packets.Packet;

/**
 *
 * @author zgoethel12
 */
public class PacketClientConnect extends Packet
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String character;
	public String name;
	public int uuid;

	public PacketClientConnect(String character, String name, int uuid)
	{
		this.character = character;
		this.name = name;
		this.uuid = uuid;
	}

}
