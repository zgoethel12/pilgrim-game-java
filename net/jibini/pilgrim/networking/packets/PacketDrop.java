package net.jibini.pilgrim.networking.packets;

import net.jibini.networking.packets.Packet;

/**
 *
 * @author zgoethel12
 */
public class PacketDrop extends Packet
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public float x, y, vx, vy;
	public int tracker, material;

	public PacketDrop(float x, float y, float vx, float vy, int tracker, int material)
	{
		this.x = x;
		this.y = y;
		this.vx = vx;
		this.vy = vy;
		this.tracker = tracker;
		this.material = material;
	}
}
