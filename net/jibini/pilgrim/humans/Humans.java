package net.jibini.pilgrim.humans;

import net.jibini.pilgrim.Main;

/**
 *
 * @author zgoethel12
 */
public class Humans
{

	public static String[] names = new String[]
	{ "Eli", "Henning", "Jordan", "Lucas", "Sierra", "Tre", "Monk", "Zach", "Carl" };

	public static String getRandom()
	{
		return names[Main.instance.rand.nextInt(names.length)];
	}

}
