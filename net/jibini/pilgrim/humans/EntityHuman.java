package net.jibini.pilgrim.humans;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.jibini.networking.server.Server;
import net.jibini.pilgrim.Main;
import net.jibini.pilgrim.entity.EntityLiving;
import net.jibini.pilgrim.inventory.Inventory;
import net.jibini.pilgrim.inventory.ItemStack;
import net.jibini.pilgrim.materials.Material;
import net.jibini.pilgrim.networking.packets.PacketServerAnnouncement;
import net.jibini.pilgrim.tiles.Tile;
import net.jibini.platformer.DeltaTracker;
import net.jibini.platformer.character.CharacterAnimation;
import net.jibini.platformer.character.CharacterAnimator;
import net.jibini.platformer.physics.BoundingBox;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

/**
 *
 * @author zgoethel12
 */
import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.opengl.TextureImpl;

public class EntityHuman extends EntityLiving
{
	public CharacterAnimator ca;
	public String name;
	public int uuid;

	public double animCount = 0;
	public boolean inRender = false;
	public DeltaTracker tracker = new DeltaTracker();

	public Inventory inventory = new Inventory("Player Inventory", 30);

	public EntityHuman(String name, float x, float y)
	{
		super(x, y);

		inventory.addItem(new ItemStack(Material.DIRT, 99));
		inventory.addItem(new ItemStack(Material.DIRT, 99));
		inventory.addItem(new ItemStack(Material.STONE, 20));
		inventory.addItem(new ItemStack(Material.POPPER, 1041));

		this.name = name;
		uuid = Main.instance.rand.nextInt(10000) + Main.instance.rand.nextInt(10000)
				+ Main.instance.rand.nextInt(10000);

		Texture sheet = null;
		InputStream is = ResourceLoader
				.getResourceAsStream("assets/textures/characters/" + name.toLowerCase() + ".png");

		try
		{
			sheet = TextureLoader.getTexture("PNG", is, GL_NEAREST);
		} catch (IOException ex)
		{
			Logger.getLogger(Character.class.getName()).log(Level.SEVERE, null, ex);
		}

		ca = new CharacterAnimator(sheet);
		ca.setCurrentAnimation(CharacterAnimation.WALK);
		ca.setCurrentDirection(CharacterAnimation.FORWARD);
	}

	float animUpdate = 0;

	@Override
	public String getName()
	{
		return "Human";
	}

	@Override
	public int getHealth()
	{
		return 20;
	}

	public static EntityHumanMP loadSerializable(HumanSerializable hs)
	{
		EntityHumanMP result = new EntityHumanMP(hs.name, hs.character, hs.x, hs.y, null);
		result.ca.current = hs.anim;
		result.ca.current.direction = hs.direction;
		result.ca.current.stage = hs.stage;
		result.uuid = hs.uuid;

		return result;
	}

	public HumanSerializable getSerializable()
	{
		String dn = Main.instance.settings.displayName;

		if (this instanceof EntityHumanMP)
		{
			dn = ((EntityHumanMP) this).displayName;
		}

		return new HumanSerializable(dn, name, x, y, ca.current, ca.current.direction, ca.current.stage, uuid);
	}

	@Override
	public void kill(String source)
	{
		String msg;

		if (source.startsWith("player"))
		{
			String killer = source.split(":")[1];
			msg = killer + " murdered %player%.";
		} else if (source.equals("fall"))
		{
			msg = "%player% fell to their death.";
		} else
		{
			msg = "Rest in peace, %player%.";
		}

		Server.globalPacket(new PacketServerAnnouncement(msg), true);
		ca.setCurrentAnimation(CharacterAnimation.HURT);
	}

	@Override
	public void update()
	{
		double delta = tracker.getDeltaTime();

		try
		{
			if (Main.instance.gui == null)
			{
				boolean animating = false;
				boolean resetting = true;

				if ((Keyboard.isKeyDown(Keyboard.KEY_W) || Keyboard.isKeyDown(Keyboard.KEY_SPACE)) && mr.down)
				{
					setVelocity(vx, 7.25f);
				}

				float side = 4.75f;

				if (mr.down)
				{
					if (Keyboard.isKeyDown(Keyboard.KEY_A))
					{
						setVelocity(-side, vy);
						ca.setCurrentAnimation(CharacterAnimation.WALK);
						ca.setCurrentDirection(CharacterAnimation.LEFT);
						animating = true;
						resetting = false;
					}

					if (Keyboard.isKeyDown(Keyboard.KEY_D))
					{
						setVelocity(side, vy);
						ca.setCurrentAnimation(CharacterAnimation.WALK);
						ca.setCurrentDirection(CharacterAnimation.RIGHT);
						animating = true;
						resetting = false;
					}
				} else
				{
					resetting = false;
					int anim = 7;

					if (Keyboard.isKeyDown(Keyboard.KEY_A))
					{
						applyForce((float) ((-side / 25) * delta * 100), 0);
						ca.setCurrentAnimation(CharacterAnimation.WALK);
						ca.setCurrentDirection(CharacterAnimation.LEFT);
						ca.current.stage = anim;
					}

					if (Keyboard.isKeyDown(Keyboard.KEY_D))
					{
						applyForce((float) ((side / 25) * delta * 100), 0);
						ca.setCurrentAnimation(CharacterAnimation.WALK);
						ca.setCurrentDirection(CharacterAnimation.RIGHT);
						ca.current.stage = anim;
					}

					if (vx > side)
					{
						vx = side;
					} else if (vx < -side)
					{
						vx = -side;
					}
				}

				if (animating)
				{
					animCount += delta;

					if (animCount >= 0.1 && !inRender)
					{
						animCount = 0;
						ca.step();
					}
				}

				if ((resetting || mr.left || mr.right) && ca.current.getID().equals(CharacterAnimation.WALK))
				{
					ca.reset();
				}
			}
		} catch (Exception ex)
		{

		}
	}

	@Override
	public void render()
	{
		Main.instance.game.delayedRender.add(new Runnable()
		{
			@Override
			public void run()
			{
				inRender = true;

				glPushMatrix();
				boolean notMP = !(EntityHuman.this instanceof EntityHumanMP);

				if (notMP)
				{
					glLoadIdentity();
					float z = Main.instance.game.zoom;
					glScalef(z, z, z);
				}

				glTranslatef(0, 0, 5);

				if (notMP)
				{
					ca.quad(-100, -75, 200, 200);
				} else
				{
					glTranslatef(0, 0, -0.5f);
					ca.quad(x - 100, y, 200, 200);
				}

				glPopMatrix();

				inRender = false;
			}
		});
	}

	public void renderHotbar()
	{
		glTranslatef(5, 10, 0);

		for (int i = 0; i < 10; i++)
		{
			ItemStack slot = inventory.getItem(i);

			glTranslatef(10, 0, 0);

			if (slot != null)
			{
				glDisable(GL_CULL_FACE);
				Tile.renderTile(i, 0, slot.type.texturing(Material.AIR, Material.AIR, Material.AIR, Material.AIR));
				glEnable(GL_CULL_FACE);

				String text = "x" + slot.count;
				int length = Main.instance.fontRenderer.getWidth(text);

				glTranslatef(0, 0, 1);
				Main.instance.fontRenderer.drawString((i + 1) * Tile.TILE_SIZE - (length + 5), Tile.TILE_SIZE, text);
				TextureImpl.bindNone();

				glTranslatef(0, 0, -1);
			}
		}

		glTranslatef((-10 * 10) - 5, -10, 0);
	}

	@Override
	public BoundingBox getBoundingBox()
	{
		return new BoundingBox(x - 30, y + 11, 60, 155);
	}

	@Override
	public boolean isStatic()
	{
		return false;
	}

	@Override
	public boolean isGhost()
	{
		return true;
	}
}
