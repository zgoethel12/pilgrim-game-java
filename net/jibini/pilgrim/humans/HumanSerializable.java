package net.jibini.pilgrim.humans;

import java.io.Serializable;
import net.jibini.platformer.character.CharacterAnimation;

/**
 *
 * @author zgoethel12
 */
public class HumanSerializable implements Serializable
{

	private static final long serialVersionUID = 6529685098267757690L;

	public String name, character;
	public float x, y;
	public CharacterAnimation anim;
	public int direction, stage, uuid;

	public HumanSerializable(String name, String character, float x, float y, CharacterAnimation anim, int direction,
			int stage, int uuid)
	{
		this.name = name;
		this.character = character;
		this.x = x;
		this.y = y;
		this.anim = anim;
		this.direction = direction;
		this.stage = stage;
		this.uuid = uuid;
	}

}
