package net.jibini.pilgrim.humans;

import net.jibini.networking.Connection;
import net.jibini.networking.server.Server;
import net.jibini.pilgrim.Main;
import net.jibini.pilgrim.Message;
import net.jibini.pilgrim.networking.packets.PacketServerPlayerDisconnect;
import org.newdawn.slick.opengl.TextureImpl;

/**
 *
 * @author zgoethel12
 */
import static org.lwjgl.opengl.GL11.*;

public class EntityHumanMP extends EntityHuman
{

	public Connection connection;
	public String displayName;

	public EntityHumanMP(String displayName, String name, float x, float y, Connection c)
	{
		super(name, x, y);
		this.displayName = displayName;
		this.connection = c;
		noCollide = true;
	}

	@Override
	public String getName()
	{
		return "MP Human";
	}

	@Override
	public void update()
	{
		if (connection != null)
		{
			if (!connection.connected)
			{
				if (Main.instance.game.multiplayer && Main.instance.game.isServer)
				{
					Server.globalPacket(new PacketServerPlayerDisconnect(uuid), true);

					Main.instance.game.messages.add(new Message(displayName + " left the game."));
				}
				Main.instance.game.removePlayer(this);
			}
		}
	}

	@Override
	public void render()
	{
		super.render();
		glPushMatrix();
		glScalef(1, -1, 1);
		glTranslatef(0, 0, 6);
		float w = Main.instance.fontRenderer.getWidth(displayName);
		Main.instance.fontRenderer.drawString(x - w / 2, -y - (getBoundingBox().h + 50), displayName);
		TextureImpl.bindNone();
		glPopMatrix();
	}

	@Override
	public boolean isStatic()
	{
		return true;
	}

}
