package net.jibini.pilgrim.inventory;

import java.io.Serializable;
import net.jibini.pilgrim.materials.Material;

/**
 *
 * @author zgoethel12
 */
public class ItemStack implements Serializable
{
	private static final long serialVersionUID = 6529685098267757690L;

	public Material type;
	public int count;

	public ItemStack(Material type, int count)
	{
		this.type = type;
		this.count = count;
	}
}
