package net.jibini.pilgrim.inventory;

import java.io.Serializable;
import net.jibini.pilgrim.extensions.HashMap;

/**
 *
 * @author zgoethel12
 */
public class Inventory implements Serializable
{
	private static final long serialVersionUID = 6529685098267757690L;

	public HashMap<Integer, ItemSlot> slots = new HashMap<Integer, ItemSlot>();
	public String title;
	public int maxSlots;

	public Inventory(String title, int slots)
	{
		this.title = title;
		this.maxSlots = slots;

		for (int i = 0; i < maxSlots; i++)
		{
			this.slots.put(i, new ItemSlot(i, null));
		}
	}

	public boolean isFull()
	{
		boolean full = true;

		for (ItemSlot slot : slots.values())
		{
			if (slot.stack == null)
			{
				full = false;
			}
		}

		return full;
	}

	public boolean addItem(ItemStack added)
	{
		/*
		 * if (!isFull()) { ItemSlot slot = slots.get(getNextEmpty());
		 * slot.stack = added; }
		 * 
		 * return !isFull();
		 */

		for (ItemSlot slot : slots.values())
		{
			if (slot.stack != null)
			{
				if (slot.stack.type.equals(added.type))
				{
					if (slot.stack.count < 100)
					{
						for (int i = 0; i < added.count; i++)
						{
							slot.stack.count++;

							if (slot.stack.count >= 100)
							{
								int diff = (added.count + slot.stack.count) % 100 - 1;
								if (diff != 0)
									setItem(getNextEmpty(), new ItemStack(added.type, diff));
								return true;
							}
						}

						return true;
					}
				}
			}
		}

		if (!isFull())
		{
			setItem(getNextEmpty(), added);

			return true;
		}

		return false;
	}

	public ItemStack getItem(int slot)
	{
		return slots.get(slot).stack;
	}

	public void setItem(int slot, ItemStack stack)
	{
		slots.get(slot).stack = stack;
	}

	public int getNextEmpty()
	{
		for (int i = 0; i < maxSlots; i++)
		{
			if (getItem(i) == null)
			{
				return i;
			}
		}

		return -1;
	}
}
