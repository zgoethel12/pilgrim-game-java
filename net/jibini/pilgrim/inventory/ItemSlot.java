package net.jibini.pilgrim.inventory;

import java.io.Serializable;

/**
 *
 * @author zgoethel12
 */
public class ItemSlot implements Serializable
{
	private static final long serialVersionUID = 6529685098267757690L;

	public int slot;
	public ItemStack stack;

	public ItemSlot(int slot, ItemStack stack)
	{
		this.slot = slot;
		this.stack = stack;
	}
}
