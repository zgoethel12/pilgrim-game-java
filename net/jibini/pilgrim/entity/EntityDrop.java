package net.jibini.pilgrim.entity;

import net.jibini.networking.client.Client;
import net.jibini.networking.server.Server;
import net.jibini.pilgrim.Main;
import net.jibini.pilgrim.assets.Sounds;
import net.jibini.pilgrim.inventory.ItemStack;
import net.jibini.pilgrim.materials.Material;
import net.jibini.pilgrim.networking.packets.PacketKillDrop;
import net.jibini.pilgrim.tiles.Tile;
import net.jibini.platformer.DeltaTracker;
import net.jibini.platformer.physics.BoundingBox;
import net.jibini.platformer.physics.MovementRestrictions;

/**
 *
 * @author zgoethel12
 */
import static org.lwjgl.opengl.GL11.*;

public class EntityDrop extends Entity
{
	public Material m;
	public double s = 4;
	public int tracker;
	public DeltaTracker d_tracker = new DeltaTracker();

	public EntityDrop(float x, float y, float vx, float vy, Material m, int tracker)
	{
		super(x, y);
		// this.vx = (Main.instance.rand.nextFloat() - 0.5f) * 3;
		this.vx = vx;
		// this.vy = Main.instance.rand.nextFloat() * 1.5f;
		this.vy = vy;
		this.tracker = tracker;
		this.m = m;
	}

	@Override
	public String getName()
	{
		return "Item Drop";
	}

	@Override
	public void update()
	{
		s += d_tracker.getDeltaTime();
		MovementRestrictions mr = this.collides(Main.instance.game.player);

		if (mr.up || mr.down || mr.left || mr.right)
		{
			if (Main.instance.game.multiplayer)
			{
				if (Main.instance.game.isServer)
				{
					Server.globalPacket(new PacketKillDrop(tracker), true);
				} else
				{
					Client.sendPacket(new PacketKillDrop(tracker));
				}
			}

			Main.instance.objects.remove(this);
			Sounds.CLICK.play();
			Main.instance.game.player.inventory.addItem(new ItemStack(m, 1));
		}
	}

	@Override
	public void render()
	{
		glPushMatrix();
		glScalef(0.5f, 0.5f, 0.5f);
		glTranslatef(x, y, 5);
		double sin = Math.sin(s);
		Tile.renderTile(x / Tile.TILE_SIZE, (y + ((float) sin + 1) * 25) / Tile.TILE_SIZE,
				m.texturing(Material.STONE, Material.STONE, Material.STONE, Material.STONE));
		glPopMatrix();
	}

	@Override
	public BoundingBox getBoundingBox()
	{
		return new BoundingBox(x, y, 25, 25);
	}
}
