package net.jibini.pilgrim.entity;

import net.jibini.pilgrim.Main;

/**
 *
 * @author zgoethel12
 */
public abstract class EntityLiving extends Entity
{

	public EntityLiving(float x, float y)
	{
		super(x, y);
	}

	public int health = getHealth();

	public abstract int getHealth();

	public void kill(String source)
	{
		try
		{
			Main.instance.objects.remove(this);
		} catch (Exception ex)
		{
			System.out.println("Could not remove Living Entity.");
		}
	}

	public void damage(float amount, String source)
	{
		health -= amount;
		if (health <= 0)
		{
			health = 0;
			kill(source);
		}
	}

}
