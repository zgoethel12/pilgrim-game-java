package net.jibini.pilgrim.entity;

import net.jibini.platformer.objects.GameObject;

/**
 *
 * @author zgoethel12
 */
public abstract class Entity extends GameObject
{

	private static int nextID = -1;

	public static int getNextID()
	{
		nextID++;
		return nextID;
	}

	private int id;

	public Entity(float x, float y)
	{
		id = getNextID();
		this.x = x;
		this.y = y;
	}

	public int getID()
	{
		return id;
	}

	public void overrideID(int id)
	{
		this.id = id;
	}

	public abstract String getName();

	@Override
	public boolean isStatic()
	{
		return false;
	}

	@Override
	public boolean isGhost()
	{
		return true;
	}

}
