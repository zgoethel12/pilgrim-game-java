package net.jibini.pilgrim.objects;

import net.jibini.pilgrim.Main;
import net.jibini.pilgrim.assets.Textures;
import net.jibini.platformer.DeltaTracker;
import net.jibini.platformer.objects.GameObject;
import net.jibini.platformer.physics.BoundingBox;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.opengl.TextureImpl;

/**
 *
 * @author zgoethel12
 */
import static org.lwjgl.opengl.GL11.*;

public class Cloud extends GameObject
{
	public static final int CLOUD_SIZE = 1000;
	public DeltaTracker tracker = new DeltaTracker();

	public Cloud()
	{
		this.x = Display.getWidth() / 2 / Main.instance.game.zoom + Main.instance.game.player.x;
		this.y = 4000 + Main.instance.rand.nextInt(1000);
	}

	@Override
	public void update()
	{
		x -= tracker.getDeltaTime() * 50;

		if (x + CLOUD_SIZE < -Display.getWidth() / 2 / Main.instance.game.zoom + Main.instance.game.player.x)
		{
			this.x = Display.getWidth() / 2 / Main.instance.game.zoom + Main.instance.game.player.x;
			this.y = 4000 + Main.instance.rand.nextInt(1000);
		}
	}

	@Override
	public void render()
	{
		Textures.CLOUD.bind();
		glBegin(GL_QUADS);
		glTexCoord2f(0, 1);
		glVertex3f(x, y, -5);
		glTexCoord2f(1, 1);
		glVertex3f(x + CLOUD_SIZE, y, -5);
		glTexCoord2f(1, 0);
		glVertex3f(x + CLOUD_SIZE, y + CLOUD_SIZE, -5);
		glTexCoord2f(0, 0);
		glVertex3f(x, y + CLOUD_SIZE, -5);
		glEnd();
		TextureImpl.bindNone();
	}

	@Override
	public BoundingBox getBoundingBox()
	{
		return new BoundingBox(0, 0, 0, 0);
	}

	@Override
	public boolean isStatic()
	{
		return true;
	}

	@Override
	public boolean isGhost()
	{
		return true;
	}
}
