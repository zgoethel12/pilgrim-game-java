package net.jibini.pilgrim.objects;

import net.jibini.pilgrim.Main;
import net.jibini.platformer.DeltaTracker;
import net.jibini.platformer.objects.GameObject;
import net.jibini.platformer.physics.BoundingBox;

/**
 *
 * @author zgoethel12
 */
import static org.lwjgl.opengl.GL11.*;

public class Particle extends GameObject
{
	public double life = 5;
	public float r, g, b, t = 1;
	public DeltaTracker tracker = new DeltaTracker();

	public Particle(float r, float g, float b, float x, float y, float vx, float vy)
	{
		this.r = r;
		this.g = g;
		this.b = b;
		this.x = x;
		this.y = y;
		this.vx = vx;
		this.vy = vy;
		// noCollide = true;
	}

	@Override
	public void update()
	{
		double delta = tracker.getDeltaTime();
		life -= delta;

		if (life <= 0)
		{
			t -= delta;
			// t = 0;

			if (t <= 0)
			{
				Main.instance.objects.remove(this);
			}
		}
	}

	@Override
	public void render()
	{
		glPushMatrix();
		glTranslatef(0, 0, (float) life / 5);
		glColor4f(r, g, b, t);
		glBegin(GL_QUADS);
		glVertex3f(x, y, 4);
		glVertex3f(x + 10, y, 4);
		glVertex3f(x + 10, y + 10, 4);
		glVertex3f(x, y + 10, 4);
		glEnd();
		glColor4f(1, 1, 1, 1);
		glPopMatrix();
	}

	@Override
	public BoundingBox getBoundingBox()
	{
		return new BoundingBox(x - 4, y, 18, 10);
	}

	@Override
	public boolean isStatic()
	{
		return false;
	}

	@Override
	public boolean isGhost()
	{
		return true;
	}
}
