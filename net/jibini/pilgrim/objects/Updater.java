package net.jibini.pilgrim.objects;

import java.util.ArrayList;
import java.util.Random;
import net.jibini.networking.client.Client;
import net.jibini.networking.packets.Packet;
import net.jibini.networking.server.Server;
import net.jibini.pilgrim.humans.HumanSerializable;
import net.jibini.pilgrim.Main;
import net.jibini.pilgrim.assets.Sounds;
import net.jibini.pilgrim.entity.EntityDrop;
import net.jibini.pilgrim.humans.EntityHuman;
import net.jibini.pilgrim.materials.Material;
import net.jibini.pilgrim.networking.packets.PacketClientPlayerUpdate;
import net.jibini.pilgrim.networking.packets.PacketDrop;
import net.jibini.pilgrim.networking.packets.PacketParticle;
import net.jibini.pilgrim.networking.packets.PacketServerPlayersUpdate;
import net.jibini.pilgrim.networking.packets.PacketTileUpdate;
import net.jibini.pilgrim.tiles.Tile;
import net.jibini.platformer.DeltaTracker;
import net.jibini.platformer.character.CharacterAnimation;
import net.jibini.platformer.character.animations.Slash;
import net.jibini.platformer.objects.GameObject;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author zgoethel12
 */
public class Updater
{
	public static DeltaTracker tracker = new DeltaTracker();

	public static double packetSend = 0;
	public static double breakParticle = 0;
	public static double sound = 0;
	public static double animUpdate = 0;

	public static void update()
	{
		double delta = tracker.getDeltaTime();

		// Main.instance.game.time += 1;
		if (Main.instance.game.time > Main.instance.game.maxTime)
		{
			Main.instance.game.time = 0;
		}

		packetSend += delta;

		if (packetSend >= 1d / 60)
		{
			if (!Main.instance.game.isServer)
			{
				EntityHuman p = Main.instance.game.player;
				Client.sendPacket(new PacketClientPlayerUpdate(p.x, p.y, p.ca.current, p.ca.current.direction,
						p.ca.current.stage, p.uuid));
			}

			if (Main.instance.game.multiplayer && Main.instance.game.isServer)
			{
				ArrayList<HumanSerializable> hss = new ArrayList<HumanSerializable>();

				for (GameObject go : Main.instance.objects)
				{
					if (go instanceof EntityHuman)
					{
						hss.add(((EntityHuman) go).getSerializable());
					}
				}

				Server.globalPacket(new PacketServerPlayersUpdate(hss.toArray(new HumanSerializable[0])), true);
			}

			packetSend = 0;
		}

		if (Main.instance.gui == null)
		{
			Vector2f selection = Main.instance.game.getSelectedVector();
			int x1 = (int) (selection.x / Tile.TILE_SIZE);
			int y1 = (int) (selection.y / Tile.TILE_SIZE);
			Tile selected = Main.instance.game.getTileAt(x1, y1);

			if (selected != null)
			{
				if (Main.instance.game.player.ca.current.getID().equals(CharacterAnimation.SLASH)
						&& !Mouse.isButtonDown(0))
				{
					Main.instance.game.player.ca.reset();
				}
				if (Mouse.isButtonDown(0) && Main.instance.game.player.vx == 0)
				{
					if (Main.instance.game.isInRange(x1, y1))
					{
						breakParticle += delta;
						animUpdate += delta;
						selected.strength -= delta * 75;
					} else
					{
						Main.instance.game.player.ca.reset();
					}

					Vector3f c = selected.getType().particles();

					if (animUpdate > 0.1 && selected.getType() != Material.AIR && Main.instance.game.isInRange(x1, y1))
					{
						if (!Main.instance.game.player.ca.current.getID().equals(CharacterAnimation.SLASH))
						{
							Main.instance.game.player.ca.setCurrentAnimation(CharacterAnimation.SLASH);
						}

						int max = new Slash().getSize();

						if (Mouse.getX() > Display.getWidth() / 2)
						{
							if (Main.instance.game.player.ca.current.direction != CharacterAnimation.RIGHT)
							{
								Main.instance.game.player.ca.setCurrentDirection(CharacterAnimation.RIGHT);
							}
						} else if (Mouse.getX() < Display.getWidth() / 2)
						{
							if (Main.instance.game.player.ca.current.direction != CharacterAnimation.LEFT)
							{
								Main.instance.game.player.ca.setCurrentDirection(CharacterAnimation.LEFT);
							}
						}

						Main.instance.game.player.ca.current.stage--;
						if (Main.instance.game.player.ca.current.stage < 0)
						{
							Main.instance.game.player.ca.current.stage = max - 1;
						}

						animUpdate = 0;
					} else if (animUpdate > 0.1)
					{
						Main.instance.game.player.ca.reset();
					}

					if (breakParticle > 0.1 && selected.getType() != Material.AIR
							&& Main.instance.game.isInRange(x1, y1))
					{
						Particle p = new Particle(c.x, c.y, c.z,
								selection.x + Main.instance.rand.nextInt((int) Tile.TILE_SIZE) - 10,
								selection.y + Main.instance.rand.nextInt((int) Tile.TILE_SIZE) - 10, 0, 2.5f);
						p.noCollide = true;

						if (Main.instance.game.isServer)
						{
							if (!Main.instance.settings.reducedParticles || (Main.instance.rand.nextInt(4) == 0))
							{
								if (Main.instance.settings.particleNoClip)
								{
									p.noCollide = true;
								}

								Main.instance.objects.add(p);
							}
						}

						if (Main.instance.game.multiplayer)
						{
							Packet packet = new PacketParticle(p.r, p.g, p.b, p.x, p.y, p.vx, p.vy, p.noCollide);

							if (Main.instance.game.isServer)
							{
								Server.globalPacket(packet, true);
							} else
							{
								Client.sendPacket(packet);
							}
						}

						sound += 1;

						if (sound >= 2)
						{
							Sounds.THUD.play();
							sound = 0;
						}

						breakParticle = 0;
					}

					if (selected.strength < 0 && selected.getType() != Material.AIR)
					{
						Material old = selected.getType();
						selected.setType(Material.AIR);
						breakParticle = 0;
						int particles = (old == Material.POPPER) ? 500 : 25;
						Random r = Main.instance.rand;

						for (int i = 0; i < particles; i++)
						{
							c = old.particles();
							Particle p = new Particle(c.x, c.y, c.z, selection.x + r.nextInt((int) Tile.TILE_SIZE) - 10,
									selection.y + r.nextInt((int) Tile.TILE_SIZE) - 10, (r.nextFloat() - 0.5f) * 5,
									2.5f);

							if (old == Material.POPPER)
							{
								p.vx = r.nextFloat() * 30 - 15;
								p.vy = r.nextFloat() * 30 - 15;
								p.noCollide = true;
							}

							if (Main.instance.game.isServer)
							{
								if (!Main.instance.settings.reducedParticles || (Main.instance.rand.nextInt(4) == 0))
								{
									if (Main.instance.settings.particleNoClip)
									{
										p.noCollide = true;
									}

									Main.instance.objects.add(p);
								}
							}

							if (Main.instance.game.multiplayer)
							{
								Packet packet = new PacketParticle(p.r, p.g, p.b, p.x, p.y, p.vx, p.vy, p.noCollide);

								if (Main.instance.game.isServer)
								{
									Server.globalPacket(packet, true);
								} else
								{
									Client.sendPacket(packet);
								}
							}

							if (Main.instance.game.multiplayer && Main.instance.game.isServer)
							{
								Server.globalPacket(new PacketTileUpdate(selected.parent, selected.x, selected.y,
										selected.getType().getID()), true);
							} else if (Main.instance.game.multiplayer && !Main.instance.game.isServer)
							{
								Client.sendPacket(new PacketTileUpdate(selected.parent, selected.x, selected.y,
										selected.getType().getID()));
							}
						}

						Sounds.CRUMBLE.play(0.75f, 1.5f);
						Material fin = old.dropsOther() ? Material.list.get(old.getDrop()) : old;

						if (fin != Material.AIR)
						{
							float vxp = (Main.instance.rand.nextFloat() - 0.5f) * 3;
							float vyp = Main.instance.rand.nextFloat() * 1.5f;
							boolean drop = true;
							int tracker = Main.instance.rand.nextInt() + Main.instance.rand.nextInt()
									+ Main.instance.rand.nextInt();

							if (Main.instance.game.multiplayer)
							{
								Packet send = new PacketDrop((selected.x + 0.5f) * Tile.TILE_SIZE,
										(selected.y + 0.5f) * Tile.TILE_SIZE, vxp, vyp, tracker, fin.getID());

								if (Main.instance.game.isServer)
								{
									Server.globalPacket(send, true);
								} else
								{
									Client.sendPacket(send);
									drop = false;
								}
							}

							if (drop)
							{
								Main.instance.objects.add(new EntityDrop((selected.x + 0.5f) * Tile.TILE_SIZE,
										(selected.y + 0.5f) * Tile.TILE_SIZE, vxp, vyp, fin, tracker));
							}
						}
					}
				} else
				{
					selected.strength = selected.getType().getStrength();
				}
			}
		}
	}
}
