package net.jibini.pilgrim.materials;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author zgoethel12
 */
public class Dirt extends Material
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getName()
	{
		return "Dirt";
	}

	@Override
	public boolean isLiquid()
	{
		return false;
	}

	@Override
	public Vector2f texturing(Material left, Material right, Material top, Material bottom)
	{
		return new Vector2f(1, 0);
	}

	@Override
	public boolean dropsOther()
	{
		return false;
	}

	@Override
	public int getDrop()
	{
		throw new UnsupportedOperationException("Does not drop other.");
	}

	@Override
	public int getStrength()
	{
		return 80;
	}

	@Override
	public Vector3f particles()
	{
		return new Vector3f(153f / 255, 110f / 255, 75f / 255);
	}

}
