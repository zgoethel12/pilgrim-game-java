package net.jibini.pilgrim.materials;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author zgoethel12
 */
public class Air extends Material
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getName()
	{
		return "Air";
	}

	@Override
	public boolean isLiquid()
	{
		return false;
	}

	@Override
	public Vector2f texturing(Material left, Material right, Material top, Material bottom)
	{
		return null;
	}

	@Override
	public boolean dropsOther()
	{
		return false;
	}

	@Override
	public int getDrop()
	{
		return 0;
	}

	@Override
	public int getStrength()
	{
		return 0;
	}

	@Override
	public Vector3f particles()
	{
		return new Vector3f(1, 1, 1);
	}

}
