package net.jibini.pilgrim.materials;

import net.jibini.pilgrim.Main;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author zgoethel12
 */
public class PartyPopper extends Material
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getName()
	{
		return "Party Popper";
	}

	@Override
	public boolean isLiquid()
	{
		return false;
	}

	@Override
	public Vector2f texturing(Material left, Material right, Material top, Material bottom)
	{
		return new Vector2f(6, 0);
	}

	@Override
	public boolean dropsOther()
	{
		return true;
	}

	@Override
	public int getDrop()
	{
		return Material.AIR.getID();
	}

	@Override
	public int getStrength()
	{
		return 100;
	}

	@Override
	public Vector3f particles()
	{
		return new Vector3f(Main.instance.rand.nextFloat(), Main.instance.rand.nextFloat(),
				Main.instance.rand.nextFloat());
	}

}
