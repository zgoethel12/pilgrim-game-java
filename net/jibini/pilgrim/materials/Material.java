package net.jibini.pilgrim.materials;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author zgoethel12
 */
public abstract class Material implements Serializable
{
	private static final long serialVersionUID = 6529685098267757690L;

	public static boolean initialized = false;
	public static ConcurrentHashMap<Integer, Material> list = new ConcurrentHashMap<Integer, Material>();
	public static Air AIR = new Air();
	public static Grass GRASS = new Grass();
	public static Dirt DIRT = new Dirt();
	public static Stone STONE = new Stone();
	public static PartyPopper POPPER = new PartyPopper();
	public static Brick BRICK = new Brick();

	public abstract String getName();

	public abstract boolean isLiquid();

	public abstract Vector2f texturing(Material left, Material right, Material top, Material bottom);

	public abstract boolean dropsOther();

	public abstract int getDrop();

	public abstract int getStrength();

	public abstract Vector3f particles();

	private final int id;

	public Material()
	{
		id = nextID();
	}

	public int getID()
	{
		return id;
	}

	public boolean equals(Material compare)
	{
		return compare.getID() == getID();
	}

	public static void initialize()
	{
		if (initialized)
		{
			System.out.println("Materials already initialized.");
		} else
		{
			System.out.println("Initializing materials...");
			list.put(AIR.getID(), AIR);
			list.put(GRASS.getID(), GRASS);
			list.put(DIRT.getID(), DIRT);
			list.put(STONE.getID(), STONE);
			list.put(POPPER.getID(), POPPER);
			list.put(BRICK.getID(), BRICK);
			initialized = true;
		}
	}

	public static int newID = -1;

	public static int nextID()
	{
		newID++;
		return newID;
	}
}
