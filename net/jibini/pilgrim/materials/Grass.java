package net.jibini.pilgrim.materials;

import net.jibini.pilgrim.Main;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author zgoethel12
 */
public class Grass extends Material
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getName()
	{
		return "Grass";
	}

	@Override
	public boolean isLiquid()
	{
		return false;
	}

	@Override
	public Vector2f texturing(Material left, Material right, Material top, Material bottom)
	{
		if (left != Material.AIR && right != Material.AIR)
		{
			return new Vector2f(0, 0);
		} else if (left == Material.AIR && right == Material.AIR)
		{
			return new Vector2f(5, 0);
		} else if (left == Material.AIR && right != Material.AIR)
		{
			return new Vector2f(3, 0);
		} else
		{
			return new Vector2f(4, 0);
		}
	}

	@Override
	public boolean dropsOther()
	{
		return true;
	}

	@Override
	public int getDrop()
	{
		return Material.DIRT.getID();
	}

	@Override
	public int getStrength()
	{
		return 100;
	}

	@Override
	public Vector3f particles()
	{
		return Main.instance.rand.nextBoolean() ? new Vector3f(0, 0.75f, 0) : Material.DIRT.particles();
	}

}
