package net.jibini.pilgrim.materials;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import net.jibini.pilgrim.Main;

public class Brick extends Material
{
	private static final long serialVersionUID = 4201587369316960358L;

	@Override
	public String getName()
	{
		return "Brick";
	}

	@Override
	public boolean isLiquid()
	{
		return false;
	}

	@Override
	public Vector2f texturing(Material left, Material right, Material top, Material bottom)
	{
		return new Vector2f(7, 0);
	}

	@Override
	public boolean dropsOther()
	{
		return false;
	}

	@Override
	public int getDrop()
	{
		return 0;
	}

	@Override
	public int getStrength()
	{
		return 1;
	}

	@Override
	public Vector3f particles()
	{
		Vector3f colors[] =
		{ new Vector3f(1f, 0.5f, 0.25f), new Vector3f(0.7f, 0.56f, 0.3f), new Vector3f(0.8f, 0.54f, 0.2f), };

		Vector3f result = colors[Main.instance.rand.nextInt(colors.length)];
		return result;
	}

}
