package net.jibini.pilgrim.materials;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

/**
 *
 * @author zgoethel12
 */
public class Stone extends Material
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getName()
	{
		return "Stone";
	}

	@Override
	public boolean isLiquid()
	{
		return false;
	}

	@Override
	public Vector2f texturing(Material left, Material right, Material top, Material bottom)
	{
		return new Vector2f(2, 0);
	}

	@Override
	public boolean dropsOther()
	{
		return false;
	}

	@Override
	public int getDrop()
	{
		throw new UnsupportedOperationException("Does not drop other.");
	}

	@Override
	public int getStrength()
	{
		return 1000;
	}

	@Override
	public Vector3f particles()
	{
		return new Vector3f(0.75f, 0.75f, 0.75f);
	}

}
