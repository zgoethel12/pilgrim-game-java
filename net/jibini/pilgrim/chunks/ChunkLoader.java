package net.jibini.pilgrim.chunks;

import java.io.File;
import net.jibini.pilgrim.utilities.FileHelper;

/**
 *
 * @author zgoethel12
 */
public class ChunkLoader
{

	public String world;

	public ChunkLoader(String world)
	{
		this.world = world;
	}

	public void saveChunk(Chunk chunk)
	{
		char fpc = File.separatorChar;
		FileHelper.saveObject(chunk.getSave(),
				"saves" + fpc + world + fpc + "chunks" + fpc + "chunk" + chunk.position + ".dat");
	}

	public Chunk loadChunk(int position)
	{
		char fpc = File.separatorChar;
		Object o = FileHelper.loadObject("saves" + fpc + world + fpc + "chunks" + fpc + "chunk" + position + ".dat");
		if (o == null)
		{
			return new Chunk(position, false);
		} else
		{
			Chunk c = new Chunk();
			ChunkSave cs = (ChunkSave) o;
			c.loadSave(cs, false);
			return c;
		}
	}

}
