package net.jibini.pilgrim.chunks;

import java.io.Serializable;

/**
 *
 * @author zgoethel12
 */
public class ChunkSave implements Serializable
{

	private static final long serialVersionUID = 6529685098267757690L;

	public int position;
	public int[][] tiles = new int[Chunk.WIDTH][Chunk.HEIGHT];

}
