package net.jibini.pilgrim.chunks;

import net.jibini.pilgrim.Main;
import net.jibini.pilgrim.materials.Material;
import net.jibini.pilgrim.tiles.Tile;
import net.jibini.platformer.Perlin;
import net.jibini.platformer.objects.GameObject;
import net.jibini.platformer.objects.StaticBox;
import net.jibini.platformer.physics.BoundingBox;
import net.jibini.platformer.physics.MovementRestrictions;

/**
 *
 * @author zgoethel12
 */
import static org.lwjgl.opengl.GL11.*;

public class Chunk extends GameObject
{
	public static final int WIDTH = 32;
	public static final int HEIGHT = 512;

	public Tile[][] tiles = new Tile[WIDTH][HEIGHT];
	public Perlin generator;

	public boolean built = false;
	public boolean doNotBuild = false;

	public int displayList;
	public int position;
	public int seed;

	public Chunk(int position, boolean build)
	{
		this.position = position;
		this.seed = Main.instance.game.seed;

		fill();
		generate();

		if (build)
		{
			build();
		}
	}

	public Chunk()
	{

	}

	@Override
	public MovementRestrictions collides(GameObject go)
	{
		MovementRestrictions mr1 = new MovementRestrictions(false, false, false, false);

		int blockX = (int) (go.x / Tile.TILE_SIZE);
		int blockY = (int) (go.y / Tile.TILE_SIZE);
		int startX = blockX - 4;
		int endX = blockX + 4;
		int startY = blockY - 4;
		int endY = blockY + 4;

		for (int x1 = startX; x1 < endX; x1++)
		{
			for (int y1 = startY; y1 < endY; y1++)
			{
				// System.out.println(x1 + " " + y1);
				Tile t = Main.instance.game.getTileAt(x1, y1)/* tiles[x1][y1] */;

				try
				{
					if (t.parent == position)
					{
						MovementRestrictions mr2 = t.collide(go);

						if (mr2.up)
						{
							mr1.up = true;
						}

						if (mr2.down)
						{
							mr1.down = true;
						}

						if (mr2.left)
						{
							mr1.left = true;
						}

						if (mr2.right)
						{
							mr1.right = true;
						}
					}
				} catch (Exception ex)
				{

				}
			}
		}

		return mr1;
	}

	public final void fill()
	{
		for (int x = 0; x < WIDTH; x++)
		{
			for (int y = 0; y < HEIGHT; y++)
			{
				tiles[x][y] = new Tile(Material.AIR, position * WIDTH + x, y, position, null);
				tiles[x][y].box = new StaticBox(tiles[x][y].x * Tile.TILE_SIZE, tiles[x][y].y * Tile.TILE_SIZE,
						Tile.TILE_SIZE, Tile.TILE_SIZE);
			}
		}
	}

	public final void generate()
	{
		generator = new Perlin(32);

		for (int x = 0; x < WIDTH; x++)
		{
			int height = generator.integral1dNoise(x + position * WIDTH + 100000000, seed) + 50;
			tiles[x][height].setType(Material.GRASS);

			for (int y = height - 1; y > height - 9; y--)
			{
				tiles[x][y].setType(Material.DIRT);
			}

			for (int y = height - 9; y > -1; y--)
			{
				tiles[x][y].setType(Material.STONE);
			}
		}
	}

	public final void build()
	{

		this.built = true;

		if (displayList != 0)
		{
			destroyList();
		}

		displayList = glGenLists(1);
		glNewList(displayList, GL_COMPILE);

		for (int x = 0; x < WIDTH; x++)
		{
			for (int y = 0; y < HEIGHT; y++)
			{
				if (tiles[x][y].getType() != Material.AIR)
				{
					Material left = Material.AIR, right = Material.AIR, top = Material.AIR, bottom = Material.AIR;

					if (x > 0)
					{
						left = tiles[x - 1][y].getType();
					} else
					{
						Chunk l = Main.instance.game.getChunk(position - 1);
						if (l != null)
						{
							left = l.tiles[WIDTH - 1][y].getType();
						}
					}

					if (x < WIDTH - 1)
					{
						right = tiles[x + 1][y].getType();
					} else
					{
						Chunk r = Main.instance.game.getChunk(position + 1);
						if (r != null)
						{
							right = r.tiles[0][y].getType();
						}
					}

					if (y > 0)
					{
						bottom = tiles[x][y - 1].getType();
					}

					if (y < HEIGHT - 1)
					{
						top = tiles[x][y + 1].getType();
					}

					Tile.renderTile(tiles[x][y].x, tiles[x][y].y,
							tiles[x][y].getType().texturing(left, right, top, bottom));
				}
			}
		}

		glEndList();
	}

	@Override
	public void render()
	{
		if (!doNotBuild)
		{
			if (!built)
			{
				build();
			}

			glPushMatrix();
			// glTranslatef(position * WIDTH * Tile.TILE_SIZE, 0, 0);
			glCallList(displayList);
			glPopMatrix();
		}
	}

	public ChunkSave getSave()
	{
		ChunkSave cs = new ChunkSave();
		cs.position = position;

		for (int x = 0; x < WIDTH; x++)
		{
			for (int y = 0; y < HEIGHT; y++)
			{
				cs.tiles[x][y] = tiles[x][y].getType().getID();
			}
		}

		return cs;
	}

	public void loadSave(ChunkSave cs, boolean build)
	{
		position = cs.position;
		fill();

		for (int x = 0; x < Chunk.WIDTH; x++)
		{
			for (int y = 0; y < Chunk.HEIGHT; y++)
			{
				tiles[x][y].setType(Material.list.get(cs.tiles[x][y]));
			}
		}

		if (build)
		{
			build();
		}
	}

	@Override
	public void update()
	{

	}

	@Override
	public BoundingBox getBoundingBox()
	{
		return new BoundingBox(0, 0, 0, 0);
	}

	@Override
	public boolean isStatic()
	{
		return true;
	}

	@Override
	public boolean isGhost()
	{
		return false;
	}

	public void destroyList()
	{
		glDeleteLists(displayList, 1);
	}
}
