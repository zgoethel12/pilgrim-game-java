package net.jibini.pilgrim;

import java.awt.Font;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import net.jibini.networking.client.Client;
import net.jibini.networking.server.Server;
import net.jibini.pilgrim.assets.Sounds;
import net.jibini.pilgrim.assets.Textures;
import net.jibini.pilgrim.game.Game;
import net.jibini.pilgrim.game.GameLoader;
import net.jibini.pilgrim.gui.Gui;
import net.jibini.pilgrim.gui.GuiChat;
import net.jibini.pilgrim.gui.GuiDisplayMessage;
import net.jibini.pilgrim.gui.GuiLevelSelect;
import net.jibini.pilgrim.gui.GuiMainMenu;
import net.jibini.pilgrim.gui.GuiPause;
import net.jibini.pilgrim.humans.Humans;
import net.jibini.pilgrim.inventory.Inventory;
import net.jibini.pilgrim.materials.Material;
import net.jibini.pilgrim.networking.handlers.ClientHandler;
import net.jibini.pilgrim.networking.packets.PacketClientChunkRequest;
import net.jibini.pilgrim.networking.packets.PacketClientConnect;
import net.jibini.pilgrim.networking.packets.PacketClientPoll;
import net.jibini.pilgrim.tiles.Tile;
import net.jibini.pilgrim.utilities.FileHelper;
import net.jibini.pilgrim.utilities.MouseHelper;
import net.jibini.platformer.GameLoop;
import net.jibini.platformer.physics.PhysicsThread;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.TextureImpl;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.opengl.ImageIOImageData;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.util.ResourceLoader;

/**
 *
 * @author zgoethel12
 */
import static org.lwjgl.opengl.GL11.*;

public class Main extends GameLoop
{
	public static Main instance;

	public static void main(String[] args)
	{
		try
		{
			Display.setIcon(new ByteBuffer[]
			{ new ImageIOImageData().loadImage(ResourceLoader.getResourceAsStream("assets/textures/ps.png"), false,
					null),
					new ImageIOImageData().loadImage(ResourceLoader.getResourceAsStream("assets/textures/ps16.png"),
							false, null) });
		} catch (IOException ex)
		{
			ex.toString();
		}

		instance = new Main();
		instance.start();
	}

	public Game game = null;
	public Gui gui = null;

	public TrueTypeFont fontRenderer;
	public CopyOnWriteArrayList<Runnable> mainThread = new CopyOnWriteArrayList<Runnable>();
	public CopyOnWriteArrayList<Integer> overriddenUUIDs = new CopyOnWriteArrayList<Integer>();
	public SettingsSave settings;
	public Random rand;

	public Main()
	{
		// LogHelper.override();

		name = "Pilgrim";
		version = "0.0.0";
		version_id = 237;
		credit = "Jibini Development";
	}

	public void drawSplash()
	{
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(-Display.getWidth() / 2, Display.getWidth() / 2, Display.getHeight() / 2, -Display.getHeight() / 2, -10,
				10);
		glMatrixMode(GL_MODELVIEW);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// int hw = Display.getWidth() / 2;
		int hh = Display.getHeight() / 2;
		Texture splash = Textures.loadTexture("doge_spray");
		splash.bind();

		glBegin(GL_QUADS);
		glTexCoord2f(0, 0);
		glVertex2f(-hh, -hh);
		glTexCoord2f(1, 0);
		glVertex2f(hh, -hh);
		glTexCoord2f(1, 1);
		glVertex2f(hh, hh);
		glTexCoord2f(0, 1);
		glVertex2f(-hh, hh);
		glEnd();

		Display.update();
		TextureImpl.bindNone();

		try
		{
			Thread.sleep(100);
		} catch (Exception ex)
		{

		}
	}

	public void exitGame()
	{
		System.out.println("Exiting current game.");

		if (game.isServer)
		{
			if (game.multiplayer)
			{
				System.out.println("Closing game server.");
				Server.exit();
			}

			GameLoader.saveGame(game, true);
		} else
		{
			System.out.println("Disconnecting from server.");
			Client.disconnect();
		}

		objects.clear();
		game.destroy();
		game.destroyGL();
		game = null;
	}

	@Override
	public void initGL()
	{
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_DEPTH_TEST);

		drawSplash();

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_ALPHA_TEST);
		glAlphaFunc(GL_GREATER, 0);

		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}

	@Override
	public void init()
	{
		/*
		 * Inventory inv = new Inventory("Test Inventory", 10); inv.addItem(new
		 * ItemStack(Material.AIR, 1)); inv.addItem(new ItemStack(Material.DIRT,
		 * 1)); inv.addItem(new ItemStack(Material.GRASS, 1)); inv.addItem(new
		 * ItemStack(Material.POPPER, 1)); inv.addItem(new
		 * ItemStack(Material.STONE, 1));
		 * System.out.println(inv.getItem(4).type.getName());
		 * System.out.println(inv.getNextEmpty());
		 * System.out.println(inv.isFull());
		 */

		Sounds.initialize();
		Textures.initialize();
		Material.initialize();
		Tile.initialize();

		rand = new Random();
		rand.setSeed(rand.nextInt());

		settings = (SettingsSave) FileHelper.loadObject("settings.dat");

		if (settings == null)
		{
			settings = new SettingsSave("Player", Humans.getRandom(), false, false, true);
			saveConfig();
		}

		Font temp001 = new Font("Standard", Font.PLAIN, 20);
		fontRenderer = new TrueTypeFont(temp001, true);

		gui = new GuiMainMenu();

		// PhysicsThread.begin(this);
		PhysicsThread.game = this;
		TextureImpl.bindNone();

		MouseHelper.listener = new MouseHelper.MListener()
		{
			@Override
			public void onMouse(int button, boolean state)
			{
				if (gui != null)
				{
					gui.mouse(button, state);
				} else
				{
					if (game != null)
					{
						game.handleMouse(button, state);
					}
				}
			}
		};
	}

	@Override
	public void update()
	{
		MouseHelper.update();

		if (game != null)
		{
			game.update();

			if (pressedI.contains(Keyboard.KEY_ESCAPE))
			{
				if (gui == null)
				{
					try
					{
						gui = new GuiPause();
					} catch (ExceptionInInitializerError err)
					{
						System.out.println(err.getCause());
					}
				} else
				{
					if (game != null)
					{
						gui = null;
					}
				}
			}

			if (pressedI.contains(Keyboard.KEY_T) && gui == null && game.multiplayer)
			{
				gui = new GuiChat();
			}
		}

		if (mainThread.size() > 0)
		{
			Runnable r = mainThread.get(0);
			r.run();
			mainThread.remove(r);
		}

		PhysicsThread.update();
	}

	@Override
	public void render()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();

		if (game != null)
		{
			game.render();
		}

		gui();

		if (gui != null)
		{
			glPushMatrix();
			glTranslatef(0, 0, 900);
			gui.render();
			glPopMatrix();
		}

		glFlush();
	}

	@Override
	public void destroy()
	{
		if (game != null)
		{
			if (game.isServer)
			{
				GameLoader.saveGame(game, true);
			}
		}
	}

	public void saveConfig()
	{
		FileHelper.saveObject(settings, "settings.dat");
	}

	@Override
	public void destroyGL()
	{

	}

	public void startGame(String name, boolean multiplayer, boolean isServer)
	{
		Game g = GameLoader.loadGame(name, multiplayer);

		if (g == null)
		{
			g = new Game(name, rand.nextInt(1000000), multiplayer, isServer, new Inventory("Player Inventory", 30),
					null);
		}

		game = g;
		g.initGL();
		g.init();
	}

	public void startServer()
	{
		gui = new GuiLevelSelect(true, true);
	}

	public void connectToServer(String url)
	{
		System.out.println("Connecting to: " + url);
		String result;
		result = Client.connect(url, 25566, new ClientHandler());

		if (result.equalsIgnoreCase("Good"))
		{
			mainThread.add(new Runnable()
			{
				@Override
				public void run()
				{
					Client.sendPacket(new PacketClientPoll());
					game = new Game(null, 0, true, false, new Inventory("Player Inventory", 30), null);
					gui = null;
					game.initGL();
					game.init();
					Client.sendPacket(
							new PacketClientConnect(settings.character, settings.displayName, game.player.uuid));
					Client.sendPacket(new PacketClientChunkRequest(0));
				}
			});
		} else
		{
			gui = new GuiDisplayMessage(result);
		}
	}

	public float distance(Vector2f a, Vector2f b)
	{
		a.translate(0 - b.x, 0 - b.y);
		return a.length();
	}
}
