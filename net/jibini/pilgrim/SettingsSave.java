package net.jibini.pilgrim;

import java.io.Serializable;

/**
 *
 * @author zgoethel12
 */
public class SettingsSave implements Serializable
{
	private static final long serialVersionUID = 6529685098267757690L;

	public String displayName, character;
	public boolean particleNoClip, reducedParticles, vignette;

	public SettingsSave(String displayName, String character, boolean particleNoClip, boolean reducedParticles,
			boolean vignette)
	{
		this.displayName = displayName;
		this.character = character;
		this.particleNoClip = particleNoClip;
		this.reducedParticles = reducedParticles;
		this.vignette = vignette;
	}
}
