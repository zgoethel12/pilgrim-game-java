package net.jibini.pilgrim.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author zgoethel12
 */
public class FileHelper
{

	public static String getWorkingDirectory()
	{
		String os = (System.getProperty("os.name")).toUpperCase();
		if (os.contains("WIN"))
		{
			return System.getenv("AppData");
		} else
		{
			return System.getProperty("user.home") + "/Library/Application Support";
		}
	}

	public static void saveObject(Object object, String path)
	{
		try
		{
			String wd = getWorkingDirectory();
			File file = new File(wd + File.separatorChar + ".pilgrim" + File.separatorChar + path);
			File parent = file.getParentFile();
			parent.mkdirs();
			file.createNewFile();
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(object);
			oos.flush();
			oos.close();
		} catch (IOException ex)
		{
			Logger.getLogger(FileHelper.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static Object loadObject(String path)
	{
		try
		{
			String wd = getWorkingDirectory();
			File file = new File(wd + File.separatorChar + ".pilgrim" + File.separatorChar + path);
			if (file.exists())
			{
				FileInputStream fis = new FileInputStream(file);
				ObjectInputStream ois = new ObjectInputStream(fis);
				Object r = ois.readObject();
				ois.close();
				return r;
			} else
			{
				return null;
			}
		} catch (IOException ex)
		{
			Logger.getLogger(FileHelper.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		} catch (ClassNotFoundException ex)
		{
			Logger.getLogger(FileHelper.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
	}

}
