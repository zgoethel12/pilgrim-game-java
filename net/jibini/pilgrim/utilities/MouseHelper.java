package net.jibini.pilgrim.utilities;

import org.lwjgl.input.Mouse;

/**
 *
 * @author zgoethel12
 */
public class MouseHelper
{

	public static MListener listener;

	public static void update()
	{
		while (Mouse.next())
		{
			int button = Mouse.getEventButton();
			boolean state = Mouse.getEventButtonState();
			if (button >= 0)
			{
				listener.onMouse(button, state);
			}
		}
	}

	public static abstract class MListener
	{
		public abstract void onMouse(int button, boolean state);
	}

}
