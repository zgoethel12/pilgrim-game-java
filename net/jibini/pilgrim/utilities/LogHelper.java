package net.jibini.pilgrim.utilities;

import java.io.PrintStream;
import net.jibini.pilgrim.Main;

/**
 *
 * @author zgoethel12
 */
public class LogHelper
{
	public static void override()
	{
		System.setOut(new PrintStream(System.out)
		{
			@Override
			public void println(String text)
			{
				super.println("[" + Main.instance.name + "/INFO]: " + text);
			}
		});
	}
}
