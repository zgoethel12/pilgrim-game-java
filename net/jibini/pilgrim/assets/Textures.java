package net.jibini.pilgrim.assets;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

/**
 *
 * @author zgoethel12
 */
import static org.lwjgl.opengl.GL11.GL_NEAREST;

public class Textures
{

	public static Texture HELPER_BOX;
	public static Texture SKY_GRADIENT;
	public static Texture TILES;
	public static Texture LOGO;
	public static Texture HEALTH;
	public static Texture CLOUD;
	public static Texture VIGNETTE;

	public static boolean initialized;

	public static void initialize()
	{
		if (initialized)
		{
			System.out.println("Texture assets already initialized.");
		} else
		{
			System.out.println("Initializing texture assets...");
			{
				HELPER_BOX = loadTexture("hud/helperbox");
				SKY_GRADIENT = loadTexture("skygradient");
				TILES = loadTexture("tiles");
				LOGO = loadTexture("logo");
				HEALTH = loadTexture("hud/health");
				CLOUD = loadTexture("cloud");
				VIGNETTE = loadTexture("hud/vignette");
			}

			initialized = true;
		}
	}

	public static Texture loadTexture(String path)
	{
		try
		{
			path = "assets/textures/" + path + ".png";
			return TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(path), GL_NEAREST);
		} catch (IOException ex)
		{
			Logger.getLogger(Textures.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
	}

}
