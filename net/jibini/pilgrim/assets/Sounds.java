package net.jibini.pilgrim.assets;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

/**
 *
 * @author zgoethel12
 */
public class Sounds
{

	public static Sound PICK, THUD, CRUMBLE, CLICK;

	public static boolean initialized;

	public static void initialize()
	{
		if (initialized)
		{
			System.out.println("Sound assets already initialized.");
		} else
		{
			System.out.println("Initializing sound assets...");
			{
				PICK = loadSound("pick");
				THUD = loadSound("thud");
				CRUMBLE = loadSound("crumble");
				CLICK = loadSound("click");
			}
			initialized = true;
		}
	}

	public static Sound loadSound(String path)
	{
		try
		{
			return new Sound("assets/sounds/" + path + ".ogg");
		} catch (SlickException ex)
		{
			Logger.getLogger(Sounds.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		}
	}

}
